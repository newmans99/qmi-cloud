
const Queue = require('bull');

export const TF_APPLY_QUEUE = 'TF_APPLY_QUEUE';
export const TF_DESTROY_QUEUE = 'TF_DESTROY_QUEUE';
export const TF_APPLY_QSEOK_QUEUE = 'TF_APPLY_QSEOK_QUEUE';


var terraformApplyQueue = new Queue(TF_APPLY_QUEUE, process.env.REDIS_URL);
var terraformDestroyQueue = new Queue(TF_DESTROY_QUEUE, process.env.REDIS_URL);
var terraformApplyQseokQueue = new Queue(TF_APPLY_QSEOK_QUEUE, process.env.REDIS_URL);



export const queues = {
  [TF_APPLY_QUEUE]: terraformApplyQueue,
  [TF_DESTROY_QUEUE]: terraformDestroyQueue,
  [TF_APPLY_QSEOK_QUEUE]: terraformApplyQseokQueue
};


for (let key in queues) {
  queues[key].on('completed', function(job, result) {
    console.log(`Job ${job.id} completed! Result`, result);
  });
  
  
  queues[key].on('error', function(error) {
    // An error occured.
  });
  
  queues[key].on('waiting', function(jobId){
    console.log(`Job ${jobId} is waiting...`);
    // A Job is waiting to be processed as soon as a worker is idling.
  });
  
  queues[key].on('active', function(job, jobPromise){
    // A job has started. You can use `jobPromise.cancel()`` to abort it.
    console.log(`Job ${job.id} is in active`);
  });
  
  queues[key].on('stalled', function(job){
    // A job has been marked as stalled. This is useful for debugging job
    // workers that crash or pause the event loop.
  });
  
  queues[key].on('progress', function(job, progress){
    // A job's progress was updated!
    console.log(`Job ${job.id} is ${progress * 100}% ready!`);
  });
  
  queues[key].on('failed', function(job, err){
    // A job failed with reason `err`!
    console.log(`Job ${job.id} has failed:`, err);
  });
}

