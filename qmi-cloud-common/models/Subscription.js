const mongoose = require('mongoose')
mongoose.set('useFindAndModify', false);
//mongoose.set('debug', true)

const subSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    subsId: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    vnetExists: {
        type: Boolean,
        required: true,
        default: false
    },
    subnetId: {
        type: String
    },
    appGwSubnetId: {
        type: String
    }
});


module.exports = mongoose.model('Subscription', subSchema);