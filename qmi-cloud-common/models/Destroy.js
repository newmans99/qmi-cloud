const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
//mongoose.set('debug', true)


const destroySchema = new mongoose.Schema({
    user: {
        type: mongoose.Types.ObjectId, ref: 'User'
    },
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    status: {
        type: String,
        default: "queued"
    },
    logFile: String,
    jobId: String
});


module.exports = mongoose.model('Destroy', destroySchema)