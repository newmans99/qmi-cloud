const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
const crypto = require("crypto");
//mongoose.set('debug', true)


const schema = new mongoose.Schema({
    user: {
        type: mongoose.Types.ObjectId, ref: 'User'
    },
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    isActive: {
        type: Boolean,
        default: true
    },
    apiKey: {
        type: String,
        default: function() {
            return crypto.randomBytes(64).toString('hex');
        },
        index: true
    }
});


module.exports = mongoose.model('ApiKey', schema)