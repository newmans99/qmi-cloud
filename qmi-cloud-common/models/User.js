const mongoose = require('mongoose')
mongoose.set('useFindAndModify', false);
//mongoose.set('debug', true)


const userSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    displayName: String,
    upn: String,
    oid: {
        type: String,
        index: true
    },
    role: {
        type: String,
        default: "user"
    },
    lastLogin: {
        type: Date
    },
});


module.exports = mongoose.model('User', userSchema);