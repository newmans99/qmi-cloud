const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
//mongoose.set('debug', true)


const sc = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    type: {
        type: String
    },
    message: {
        type: String
    },
    provision: {
        type: mongoose.Types.ObjectId, ref: 'Provision',
        index: true
    }
});


module.exports = mongoose.model('Notification', sc)