const mongoose = require('mongoose')
mongoose.set('useFindAndModify', false);
//mongoose.set('debug', true)


const scenarioSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    version: {
        type: String,
        requred: true
    },
    name: {
        type: String,
        unique : true,
        requred: true,
        index: true
    },
    isAdminOnly: {
        type: Boolean,
        default: false
    },
    isWafPolicyAppGw: {
        type: Boolean,
        default: false
    },
    isExternal: {
        type: Boolean,
        default: false
    },
    isDisabled: {
        type: Boolean,
        default: false
    },
    title: {
        type: String,
        requred: true
    },
    description: String,
    availableProductVersions: Array,
    productVersionDefault: String,
    newImageName: String, //For Gen scenarios
    subscription: {
        type: mongoose.Types.ObjectId, ref: 'Subscription',
        requred: true
    }
});


module.exports = mongoose.model('Scenario', scenarioSchema)