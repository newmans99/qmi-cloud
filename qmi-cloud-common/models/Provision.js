const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);
//mongoose.set('debug', true)

const provisionSchema = new mongoose.Schema({
    user: {
        type: mongoose.Types.ObjectId, ref: 'User',
        index: true
    },
    created: {
        type: Date,
        default: Date.now,
        index : true
    },
    updated: {
        type: Date,
        default: Date.now
    },
    scenario: String,
    scenarioVersion: {
        type: String,
        default: "1.0"
    },
    description: String,
    vmImage: Object,
    status: {
        type: String,
        default: "queued"
    },
    jobId: String,
    logFile: String,
    outputs: Object,
    path: String,
    isExternalAccess: {
        type: Boolean,
        default: false
    },
    isDestroyed: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false,
        index: true
    },
    statusVms: {
        type: String
    },
    destroy: {
        type: mongoose.Types.ObjectId, ref: 'Destroy'
    },
    actualDestroyDate: {
        type: Date
    },
    runningFrom: {
        type: Date
    },
    stoppedFrom: {
        type: Date
    },
    timeRunning: {
        type: Number,
        default: 0
    },
    countExtend: {
        type: Number,
        default: 0
    },
    pendingNextAction: {
        type: String
    }
});


module.exports = mongoose.model('Provision', provisionSchema)