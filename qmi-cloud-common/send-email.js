'use strict';
const nodemailer = require('nodemailer');
const FROM = '"Qlik" <no-reply@qlik.com>';
var transporter;

const RUNNING_PERIOD = 4;
const RUNNING_PERIOD_WARNING_DAYS = 1;
const STOP_PERIOD = 20;
const STOP_PERIOD_WARNING_DAYS = 2;


const RUNNING_PERIOD_WARNING_HOURS = RUNNING_PERIOD_WARNING_DAYS*24;
const STOP_PERIOD_WARNING_HOURS = STOP_PERIOD_WARNING_DAYS*24;


if ( process.env.GMAIL_USERNAME && process.env.GMAIL_PASSWORD ) {
    //GMAIL
    transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 587,
        secure: false,
        auth: {
            user: process.env.GMAIL_USERNAME,
            pass: process.env.GMAIL_PASSWORD
        }
    });
} else {
    //QLIK
    transporter = nodemailer.createTransport({
        host: 'smtp.qliktech.com',
        port: 587,
        secure: false, // true for 465, false for other ports
    });
}

async function _doSend(to, subject, htmlText) {
    // send mail with defined transport object
    let info = await transporter.sendMail( {
        from: FROM, // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        text: subject, // plain text body
        html: htmlText // html body
    } );

    console.log('Provision Email ('+info.messageId+') sent to: ' + to);
}

function _getCommonDetails(provision, scenario){
    return `<div style="color:#404040;font-size:18px;margin:20px 0px">
            <p style="margin:0px">Provision information:</p>
        </div>
        <div>
            <span style="color:#404040">Purpose: </span> ${provision.description}
        </div>
        <div>
            <span style="color:#404040">Scenario: </span> ${scenario.title}
        </div>
        <div>
            <span style="color:#404040">Description: </span> ${scenario.description}
        </div>
        <div>
            <span style="color:#404040">ProvisionID: </span> ${provision._id}
        </div>`;
}

function getHtmlScenarioDestroyIn24( provision, scenario) {
    var common = _getCommonDetails(provision,scenario);
    return`<div style="width:600px;color:black!important;font-family:'Source Sans Pro',sans-serif;padding:50px">
    <div style="background-color:white;height:100%;padding:20px 10px">
        <div style="color:#404040;font-size:34px;text-align:center;margin:20px">
            <p style="margin:0px">QMI Cloud</p>
        </div>
        <div style="color:#404040;font-size:22px;margin:20px 0px 40px 0px">
            <p style="margin:0px">Provision '${scenario.title}' inactive more than ${(STOP_PERIOD - STOP_PERIOD_WARNING_DAYS)} days</p>
        </div>
        <div style="color:#404040;font-size:18px;margin:10px 0px">
            <p style="margin:0px;color: #FF2020">This scenario will be automatically DESTROYED in ${STOP_PERIOD_WARNING_HOURS} hours.</p>
        </div>
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">If you don't want this to happen, you've got ${STOP_PERIOD_WARNING_HOURS} hours (from when this email was sent) as a grace period to get back at 'Running' status this provision.</p>
        </div>
        ${common}
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">Check it out at <a href="https://qmicloud.qliktech.com">https://qmicloud.qliktech.com</a></p>
        </div>
    </div>
    </div>`;
}


function getHtmlScenarioVMsStopped( provision, scenario) {
    var common = _getCommonDetails(provision,scenario);
    return `<div style="width:600px;color:black!important;font-family:'Source Sans Pro',sans-serif;padding:50px">
    <div style="background-color:white;height:100%;padding:20px 10px">
        <div style="color:#404040;font-size:34px;text-align:center;margin:20px">
            <p style="margin:0px">QMI Cloud</p>
        </div>
        <div style="color:#404040;font-size:22px;margin:20px 0px 40px 0px">
            <p style="margin:0px">Provision '${scenario.title}'</p>
        </div>
        <div style="color:#404040;font-size:18px;margin:10px 0px">
            <p style="margin:0px;color: #FF2020">All VMs for this provision <b>stopped</b> automatically.</p>
        </div>
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">You can start them up again from <a href="https://qmicloud.qliktech.com">https://qmicloud.qliktech.com</a></p>
        </div>
        ${common}
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">Check it out at <a href="https://qmicloud.qliktech.com">https://qmicloud.qliktech.com</a></p>
        </div>
    </div>
    </div>`;
}

function getHtmlScenarioWillStopIn24( provision, scenario ) {
    var common = _getCommonDetails(provision,scenario);
    return`<div style="width:600px;color:black!important;font-family:'Source Sans Pro',sans-serif;padding:50px">
    <div style="background-color:white;height:100%;padding:20px 10px">
        <div style="color:#404040;font-size:34px;text-align:center;margin:20px">
            <p style="margin:0px">QMI Cloud</p>
        </div>
        <div style="color:#404040;font-size:22px;margin:20px 0px 40px 0px">
            <p style="margin:0px">Provision '${scenario.title}' - VMs running for ${(RUNNING_PERIOD - RUNNING_PERIOD_WARNING_DAYS)} days</p>
        </div>
        <div style="color:#404040;font-size:18px;margin:10px 0px">
            <p style="margin:0px;color: #FF2020">This scenario will automatically stop its VMs in ${RUNNING_PERIOD_WARNING_HOURS} hours.</p>
        </div>
        <div style="color:#404040;font-size:18px;margin:20px 0px 10px 0px">
            <p style="margin:0px;color: #FF2020">Take action and extend the period ${RUNNING_PERIOD} extra days.</p>
        </div>
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">If you don't want the VMs to automatically stop, you've got ${RUNNING_PERIOD_WARNING_HOURS} hours (from when this email was sent) as a grace period to extend this scenario's <b style="color: #009845">Running</b> VMs for ${RUNNING_PERIOD} extra days.</p>
        </div>
        ${common}
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">Check it out at <a href="https://qmicloud.qliktech.com">https://qmicloud.qliktech.com</a></p>
        </div>
    </div>
    </div>`;
}
function getHtmlNewProvision(provision, scenario) {
    var htmlint;
    if ( provision && provision.outputs ) {
        htmlint = `<div style="color:#404040;font-size:18px;padding: 10px 0px;">Connection resources</div>`;
    } else {
        htmlint = "";
    }
    for (let key in provision.outputs) {
        htmlint += `<div>
            <span style="color:#404040">${key}</span> 
            <pre style="color:#404040;">${provision.outputs[key]}</pre>
        </div>`;
    }

    var common = _getCommonDetails(provision, scenario);

    return `<div style="width:600px;color:black!important;font-family:'Source Sans Pro',sans-serif;padding:50px">
    <div style="background-color:white;height:100%;padding:20px 10px">
        <div style="color:#404040;font-size:34px;text-align:center;margin:20px">
            <p style="margin:0px">QMI Cloud</p>
        </div>
        <div style="color:#404040;font-size:22px;margin:20px 0px">
            <p style="margin:0px">Scenario '${scenario.title}' successfully provisioned!</p>
        </div>

        ${common}
        
        <div style="margin: 30px 0px;">
        ${htmlint}
        </div>
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">Check it out at <a href="https://qmicloud.qliktech.com">https://qmicloud.qliktech.com</a></p>
        </div>
        
    </div>
    </div>`;
}

function getHtmlErrorProvision(provision, scenario) {
    var common = _getCommonDetails(provision, scenario);
    return`<div style="width:600px;color:black!important;font-family:'Source Sans Pro',sans-serif;padding:50px">
    <div style="background-color:white;height:100%;padding:20px 10px">
        <div style="color:#404040;font-size:34px;text-align:center;margin:20px">
            <p style="margin:0px">QMI Cloud</p>
        </div>
        <div style="color:#404040;font-size:20px;margin:20px 0px">
            <p style="margin:0px;color: #FF2020">Oops! Something didn't work.</p>
        </div>
        <div style="color:#404040;font-size:20px;margin:20px 0px 50px 0px">
            <p style="margin:0px">Scenario '${scenario.title}' failed during provision.</p>
        </div>  
        ${common}
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">Check it out at <a href="https://qmicloud.qliktech.com">https://qmicloud.qliktech.com</a></p>
        </div>
    </div>
    </div>`;
}

function getHtmlDestroyProvision(provision, scenario) {
    var common = _getCommonDetails(provision, scenario);
    return`<div style="width:600px;color:black!important;font-family:'Source Sans Pro',sans-serif;padding:50px">
    <div style="background-color:white;height:100%;padding:20px 10px">
        <div style="color:#404040;font-size:34px;text-align:center;margin:20px">
            <p style="margin:0px">QMI Cloud</p>
        </div>
        <div style="color:#404040;font-size:20px;margin:40px 0px">
            <p style="margin:0px">Scenario '${scenario.title}' successfully destroyed!</p>
        </div>
        ${common}
        <div style="color:#404040;font-size:16px;margin:30px 0px">
            <p style="margin:0px">Check it out at <a href="https://qmicloud.qliktech.com">https://qmicloud.qliktech.com</a></p>
        </div>
    </div>
    </div>`;
}


// async..await is not allowed in global scope, must use a wrapper
async function send( provision, scenario ) {
    const htmlText = getHtmlNewProvision(provision, scenario);
    await _doSend(provision.user.upn, 'QMI Cloud - Provision finished successfully', htmlText);
}

async function sendError(provision, scenario ) {
    const htmlText = getHtmlErrorProvision(provision, scenario);
    await _doSend(provision.user.upn, 'QMI Cloud - Provision with failures', htmlText);
}

async function sendDestroyed(provision, scenario ) {

    const htmlText = getHtmlDestroyProvision(provision, scenario);
    await _doSend(provision.user.upn, 'QMI Cloud - Provision destroyed successfully', htmlText);
    
}

async function sendWillStopIn24( provision, scenario ) {

    const htmlText = getHtmlScenarioWillStopIn24( provision, scenario);
    await _doSend(provision.user.upn, `QMI Cloud - VMs will stop in ${RUNNING_PERIOD_WARNING_HOURS} hours`, htmlText);

}

async function sendWillDestroyIn24( provision, scenario ) {

    const htmlText = getHtmlScenarioDestroyIn24( provision, scenario);
    await _doSend(provision.user.upn, `QMI Cloud - Provision will destroy in ${STOP_PERIOD_WARNING_HOURS} hours`, htmlText);
}

async function sendVMsStopped( provision, scenario ) {
    const htmlText = getHtmlScenarioVMsStopped( provision, scenario);
    await _doSend(provision.user.upn, 'QMI Cloud - VMs stopped automatically', htmlText);
}

module.exports.send = send;
module.exports.sendError = sendError;
module.exports.sendDestroyed = sendDestroyed;
module.exports.sendWillStopIn24 = sendWillStopIn24;
module.exports.sendVMsStopped = sendVMsStopped;
module.exports.sendWillDestroyIn24 = sendWillDestroyIn24;


