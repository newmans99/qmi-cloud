# FAQ

Just an assorted selection of questions and answers.

### 1\. What can I use QMI Cloud for?

- QMI Cloud scenarios can be used to perform product demonstrations, enablement sessions and PoC environments.  It is built to aid with Customer Engagements.

* * *

### 2\. Are these QMI scenarios ready to go End-To-End demos?

- Not at this moment, a global environment is being built for PreSales by GEAR. 
- Provided scenarios are meant for (See 1). QMI works by installing licensing the products. QMI completes the main setup for the scenario to properly work where possible. You may need to put custom data, Qlik Sense applications or whatever you need for your customer engagement.

* * *

### 3\. Where are QMI Cloud resources deployed?

- All cloud resources such as Virtual Machines (VMs), disks, etc are deployed in the Qlik Azure subscription.
- For the time being, all resources are deployed in the __East US__ region in Azure. This means that it could be some significant latency or delays if you are located in another region.

* * *

### 4\. What will happen when I provision a scenario?

- Necessary resources for that scenario to work are deployed within Azure.
- An email is sent to the owner (e.g. you) indicating whether the provision was successful or with failures.
- Upon success, all credentials and access information for this provision is sent with that email.
- From that moment, VMs will keep at 'Running' status for 4 full days.
- On day 3, an email is sent warning that in the next 24 hours the VMs will automatically stop.
- You can "extend" VMs at Running status. This will renew the 'Running' period for 4 extra days. Other way, VMs will automatically stop within the next 24 hours.
- You can monitor 'Running time' and 'Time remaining until auto stop' at all times.
- __Provision will be automatically destroyed after 20 days of inactivity (20 days with Stopped VMs). On day 18 you'll get a warning email advising this will happen in 48 hours.__

* * *

### 5\. How many scenarios can I have provisioned at the same time?

- There is no limitation on the number of scenarios (or instances of the same one) provisioned at the same time. Just be aware of COST and keep VMs Stopped when possible.

- Do not forget to Destroy those provisions that you no longer need.

* * *

### 6\. Are scenarios accessible from the Internet or just from within Qlik VPN? 

- All scenarios are accessible from within the Qlik VPN.
- Some are also accessible from the Intenet (external access). This is indicated on the UI for each scenario.

* * *

### 7\. Is QMI Cloud under the 'Qlik Customer Engagement Terms' policy? 

- Yes.
- Only anonymous or public/free data can be used.
- Customer data is available with QSE SaaS, work is being done to aid PreSales manage 3rd Party data.

* * *


### 8\. Do scenarios communicate with each other? 

- Yes.
- Private IPs assigned to all VMs are reachable.
- All ports are open for these private IPs (within the Qlik VPN).


* * *

### 9\. What can I do if I don't find a scenario that suits my needs?

- You can create end-to-end more complete demos with different Qlik products using combination of multiple scenarios. 

- For example, if you needed an NPrinting demo/poc, you could use Qlik Sense scenario and Windows Blank scenario, then you can install NPrinting in this Windows Blank scenario and complete the setup yourself.


* * *

### 10\. Who is paying for these instances?

- The costs for these instances are allocated to PreSales.

- It is essential that we are all cost consciouse setup yourself.


* * *

### 11\. Can I manually stop and start the scenarios?

- Yes.


* * *

### 12\. If I shutdown the scenario, does it allocate a new IP Address?

- No.


* * *

### 13\. Are the mandatory security tools installed on these instances?

- Yes.
- Security is key for Qlik, whilst these are initially installed (Cabon Black on Windows, CrowdStrike on Linux)  it is your responsibility as the owner of this instance to ensure everything is upto date.


* * *

### 14\. Can I create new instances?

- This capability will be described soon.

* * *

### 15\. How are we managing Costs?

- Excessive use for instances, will need to be justified, every instance logged it tagged with your trigram. The usage of QMI Cloud will be made public within a Qlik Sense Application.
- GEAR is looking at estimating the price for each server that you initiate.

* * *

### 16\. My Team use a team server; can I use this as well?

- This is open to everyone, if you do have a team server, then for cost purposes, please use that server.

* * *