var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, HostListener, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent, MDBModalService } from 'angular-bootstrap-md';
import { ProvisionsService } from '../services/provisions.service';
import { AlertService } from '../services/alert.service';
import { ModalInfoComponent } from '../alert/modalinfo.component';
import { ModalConfirmComponent } from '../alert/confirm.component';
import { ScenariosService } from '../services/scenarios.service';
import { timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
var TableAdminComponent = /** @class */ (function () {
    function TableAdminComponent(modalService, _scenariosService, _alertService, cdRef, _provisionsService) {
        this.modalService = modalService;
        this._scenariosService = _scenariosService;
        this._alertService = _alertService;
        this.cdRef = cdRef;
        this._provisionsService = _provisionsService;
        this.pagingIsDisabled = false;
        this.searchText = '';
        this.selectedprov = null;
        this.showInfo = false;
        this.logShow = false;
        this.logstype = 'provision';
        this.maxVisibleItems = 10;
        this.filterParams = {
            isDestroyed: false
        };
    }
    TableAdminComponent.prototype.oninput = function () {
        this.mdbTablePagination.searchText = this.searchText;
    };
    TableAdminComponent.prototype._process = function (provisions) {
        var _this = this;
        provisions.forEach(function (p) {
            p._scenario = _this.scenarios.filter(function (s) { return s.name === p.scenario; });
            _this._provisionsService.timeRunning(p);
        });
        if (!this.provisions) {
            this.provisions = provisions;
        }
        else {
            this.provisions.forEach(function (p, index, object) {
                var found = provisions.filter(function (a) { return a._id.toString() === p._id.toString(); });
                if (found.length) {
                    p.status = found[0].status;
                    p.statusVms = found[0].statusVms;
                    p.isDestroyed = found[0].isDestroyed;
                    p.outputs = found[0].outputs;
                    p.destroy = found[0].destroy;
                    this._provisionsService.timeRunning(p);
                }
                else {
                    object.splice(index, 1);
                }
            }.bind(this));
            provisions.forEach(function (p) {
                var found = this.provisions.filter(function (a) { return a._id.toString() === p._id.toString(); });
                if (found.length === 0) {
                    this.provisions.unshift(p);
                }
            }.bind(this));
        }
        this._initElements(this.provisions);
    };
    TableAdminComponent.prototype._initElements = function (items) {
        this.mdbTable.setDataSource(items);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
    };
    TableAdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        var scenariosSub = this._scenariosService.getScenariosAll().subscribe(function (res) {
            scenariosSub.unsubscribe();
            _this.scenarios = res.results;
            _this.subscription = timer(0, 8000).pipe(switchMap(function () { return _this._provisionsService.getProvisionsAdmin(_this.filterParams); })).subscribe(function (provisions) {
                _this._process(provisions.results);
            });
        });
        this.mdbTablePagination.paginationChange().subscribe(function (data) {
            /*let page = data.last - data.first;
            if (page < this.limit && page > 0) {
              page = Math.ceil(data.last / this.limit);
              if (this.page !== page) {
                this.page = Math.ceil(data.last / this.limit);
                this.loadTable();
              }
            }*/
        });
        //this._initElements();
    };
    TableAdminComponent.prototype.ngAfterViewInit = function () {
        if (this.mdbTablePagination) {
            this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);
            this.mdbTablePagination.calculateFirstItemIndex();
            this.mdbTablePagination.calculateLastItemIndex();
            this.cdRef.detectChanges();
        }
    };
    TableAdminComponent.prototype.searchItems = function () {
        var _this = this;
        var prev = this.mdbTable.getDataSource();
        if (!this.searchText) {
            this.mdbTable.setDataSource(this.previous);
            this.elements = this.mdbTable.getDataSource();
        }
        if (this.searchText) {
            this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
            this.mdbTable.setDataSource(prev);
        }
        if (this.mdbTablePagination) {
            this.mdbTablePagination.calculateFirstItemIndex();
            this.mdbTablePagination.calculateLastItemIndex();
            this.mdbTable.searchDataObservable(this.searchText).subscribe(function () {
                _this.mdbTablePagination.calculateFirstItemIndex();
                _this.mdbTablePagination.calculateLastItemIndex();
            });
        }
    };
    TableAdminComponent.prototype.showLogs = function ($event, provision, type) {
        $event.preventDefault();
        $event.stopPropagation();
        this.logstype = type;
        this.logShow = false;
        this.selectedprov = provision;
        this.logShow = true;
    };
    TableAdminComponent.prototype.onLogsClose = function () {
        this.selectedprov = null;
        this.logShow = false;
    };
    TableAdminComponent.prototype.openConfirmStartModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-info',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm Start VMs?',
                    icon: 'play',
                    buttonColor: 'grey'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._startVms(provision);
        });
    };
    TableAdminComponent.prototype._startVms = function (provision) {
        var _this = this;
        var sub = this._provisionsService.startVms(provision._id.toString(), provision.user._id).subscribe(function (res) {
            provision.startVms = res.startVms;
            sub.unsubscribe();
            _this._alertService.showAlert({
                type: 'alert-primary',
                text: "Starting all VMs for scenario '" + provision.scenario + "'..."
            });
        });
    };
    TableAdminComponent.prototype.openConfirmStopModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-info',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm Stop VMs?',
                    icon: 'stop',
                    buttonColor: 'grey'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._stopVms(provision);
        });
    };
    TableAdminComponent.prototype._stopVms = function (provision) {
        var _this = this;
        var sub = this._provisionsService.stopVms(provision._id.toString(), provision.user._id).subscribe(function (res) {
            provision.startVms = res.startVms;
            sub.unsubscribe();
            _this._alertService.showAlert({
                type: 'alert-primary',
                text: "Stopping all VMs for scenario '" + provision.scenario + "'..."
            });
        });
    };
    TableAdminComponent.prototype.openInfoModal = function (provision) {
        this.modalService.show(ModalInfoComponent, {
            backdrop: true,
            keyboard: true,
            focus: true,
            show: false,
            ignoreBackdropClick: false,
            class: 'modal-lg',
            containerClass: '',
            animated: true,
            data: {
                info: provision
            }
        });
    };
    TableAdminComponent.prototype.openConfirmDestroyModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-danger',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm destroy this provision?',
                    icon: 'times-circle'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.newDestroy(provision._id.toString(), provision.user._id).subscribe(function (provUpdated) {
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Provision of scenario '" + provision.scenario + "' is going to be destroyed"
                });
                provision.destroy = provUpdated.destroy;
            });
        });
    };
    TableAdminComponent.prototype.openConfirmDeleteModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-danger',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm delete?',
                    icon: 'trash-alt'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.delProvision(provision._id, provision.user._id).subscribe(function (res) {
                _this.elements = _this.elements.filter(function (e) {
                    return e._id.toString() !== provision._id.toString();
                });
                _this._initElements(_this.elements);
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Provision entry '" + provision.scenario + "' was deleted"
                });
            });
        });
    };
    __decorate([
        ViewChild(MdbTablePaginationComponent, { static: true }),
        __metadata("design:type", MdbTablePaginationComponent)
    ], TableAdminComponent.prototype, "mdbTablePagination", void 0);
    __decorate([
        ViewChild(MdbTableDirective, { static: true }),
        __metadata("design:type", MdbTableDirective)
    ], TableAdminComponent.prototype, "mdbTable", void 0);
    __decorate([
        HostListener('input'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TableAdminComponent.prototype, "oninput", null);
    TableAdminComponent = __decorate([
        Component({
            selector: 'table-admin',
            templateUrl: './table-admin.component.html',
            styleUrls: ['./table-admin.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalService, ScenariosService, AlertService, ChangeDetectorRef, ProvisionsService])
    ], TableAdminComponent);
    return TableAdminComponent;
}());
export { TableAdminComponent };
//# sourceMappingURL=table-admin.component.js.map