var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { MdbTablePaginationComponent, MdbTableDirective } from 'angular-bootstrap-md';
import { Component, ViewChild, HostListener, ChangeDetectorRef } from '@angular/core';
import { UsersService } from '../services/users.service';
var TableNotificationsComponent = /** @class */ (function () {
    function TableNotificationsComponent(cdRef, _usersService) {
        this.cdRef = cdRef;
        this._usersService = _usersService;
        this.previous = [];
        this.searchText = '';
        this.maxVisibleItems = 25;
        this.loading = false;
        this.elements = [];
    }
    TableNotificationsComponent.prototype.oninput = function () {
        this.mdbTablePagination.searchText = this.searchText;
    };
    TableNotificationsComponent.prototype._initElements = function () {
        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
    };
    TableNotificationsComponent.prototype.ngOnInit = function () {
        this.refreshData();
    };
    TableNotificationsComponent.prototype.refreshData = function () {
        var _this = this;
        this.loading = true;
        this.searchText = "";
        var sub = this._usersService.getNotifications().subscribe(function (res) {
            sub.unsubscribe();
            _this.elements = res.results;
            _this.loading = false;
            _this._initElements();
        });
    };
    TableNotificationsComponent.prototype.ngAfterViewInit = function () {
        this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);
        this.mdbTablePagination.calculateFirstItemIndex();
        this.mdbTablePagination.calculateLastItemIndex();
        this.cdRef.detectChanges();
    };
    TableNotificationsComponent.prototype.searchItems = function () {
        var _this = this;
        var prev = this.mdbTable.getDataSource();
        if (!this.searchText) {
            this.mdbTable.setDataSource(this.previous);
            this.elements = this.mdbTable.getDataSource();
        }
        if (this.searchText) {
            this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
            this.mdbTable.setDataSource(prev);
        }
        this.mdbTablePagination.calculateFirstItemIndex();
        this.mdbTablePagination.calculateLastItemIndex();
        this.mdbTable.searchDataObservable(this.searchText).subscribe(function () {
            _this.mdbTablePagination.calculateFirstItemIndex();
            _this.mdbTablePagination.calculateLastItemIndex();
        });
    };
    __decorate([
        ViewChild(MdbTablePaginationComponent, { static: true }),
        __metadata("design:type", MdbTablePaginationComponent)
    ], TableNotificationsComponent.prototype, "mdbTablePagination", void 0);
    __decorate([
        ViewChild(MdbTableDirective, { static: true }),
        __metadata("design:type", MdbTableDirective)
    ], TableNotificationsComponent.prototype, "mdbTable", void 0);
    __decorate([
        HostListener('input'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TableNotificationsComponent.prototype, "oninput", null);
    TableNotificationsComponent = __decorate([
        Component({
            selector: 'table-notifications',
            templateUrl: './table-notifications.component.html',
            styleUrls: ['./table-notifications.component.scss']
        }),
        __metadata("design:paramtypes", [ChangeDetectorRef, UsersService])
    ], TableNotificationsComponent);
    return TableNotificationsComponent;
}());
export { TableNotificationsComponent };
//# sourceMappingURL=table-notifications.component.js.map