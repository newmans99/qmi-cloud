var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { MdbTablePaginationComponent, MdbTableDirective, MDBModalService } from 'angular-bootstrap-md';
import { Component, ViewChild, HostListener, ChangeDetectorRef } from '@angular/core';
import { SubscriptionModalComponent } from '../alert/edit-subscription.component';
import { SubscriptionsService } from '../services/subscriptions.service';
var TableSubsComponent = /** @class */ (function () {
    function TableSubsComponent(modalService, cdRef, _subscriptionsService) {
        this.modalService = modalService;
        this.cdRef = cdRef;
        this._subscriptionsService = _subscriptionsService;
        this.previous = [];
        this.searchText = '';
        this.maxVisibleItems = 25;
        this.loading = false;
        this.elements = [];
    }
    TableSubsComponent.prototype.oninput = function () {
        this.mdbTablePagination.searchText = this.searchText;
    };
    TableSubsComponent.prototype._initElements = function () {
        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
    };
    TableSubsComponent.prototype.ngOnInit = function () {
        this.refreshData();
    };
    TableSubsComponent.prototype.refreshData = function () {
        var _this = this;
        this.loading = true;
        this.searchText = "";
        var usersSub = this._subscriptionsService.getSubscriptions().subscribe(function (res) {
            usersSub.unsubscribe();
            _this.elements = res.results;
            _this.loading = false;
            _this._initElements();
        });
    };
    TableSubsComponent.prototype.ngAfterViewInit = function () {
        this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);
        this.mdbTablePagination.calculateFirstItemIndex();
        this.mdbTablePagination.calculateLastItemIndex();
        this.cdRef.detectChanges();
    };
    TableSubsComponent.prototype.searchItems = function () {
        var _this = this;
        var prev = this.mdbTable.getDataSource();
        if (!this.searchText) {
            this.mdbTable.setDataSource(this.previous);
            this.elements = this.mdbTable.getDataSource();
        }
        if (this.searchText) {
            this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
            this.mdbTable.setDataSource(prev);
        }
        this.mdbTablePagination.calculateFirstItemIndex();
        this.mdbTablePagination.calculateLastItemIndex();
        this.mdbTable.searchDataObservable(this.searchText).subscribe(function () {
            _this.mdbTablePagination.calculateFirstItemIndex();
            _this.mdbTablePagination.calculateLastItemIndex();
        });
    };
    TableSubsComponent.prototype.openNewSubsModal = function (subscription) {
        var _this = this;
        var modalRef = this.modalService.show(SubscriptionModalComponent, {
            class: 'modal-md modal-notify',
            containerClass: '',
            data: {
                subscription: subscription
            }
        });
        var sub = modalRef.content.action.subscribe(function (data) {
            sub.unsubscribe();
            console.log("new subs data", data);
            _this.refreshData();
        });
    };
    __decorate([
        ViewChild(MdbTablePaginationComponent, { static: true }),
        __metadata("design:type", MdbTablePaginationComponent)
    ], TableSubsComponent.prototype, "mdbTablePagination", void 0);
    __decorate([
        ViewChild(MdbTableDirective, { static: true }),
        __metadata("design:type", MdbTableDirective)
    ], TableSubsComponent.prototype, "mdbTable", void 0);
    __decorate([
        HostListener('input'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TableSubsComponent.prototype, "oninput", null);
    TableSubsComponent = __decorate([
        Component({
            selector: 'table-subscriptions',
            templateUrl: './table-subscriptions.component.html',
            styleUrls: ['./table-subscriptions.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalService, ChangeDetectorRef, SubscriptionsService])
    ], TableSubsComponent);
    return TableSubsComponent;
}());
export { TableSubsComponent };
//# sourceMappingURL=table-subs.component.js.map