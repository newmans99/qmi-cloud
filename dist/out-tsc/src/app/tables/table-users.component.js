var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { MdbTablePaginationComponent, MdbTableDirective } from 'angular-bootstrap-md';
import { Component, ViewChild, HostListener, ChangeDetectorRef } from '@angular/core';
import { AuthGuard } from '../services/auth.guard';
import { UsersService } from '../services/users.service';
var TableUsersComponent = /** @class */ (function () {
    function TableUsersComponent(cdRef, _usersService, _auth) {
        var _this = this;
        this.cdRef = cdRef;
        this._usersService = _usersService;
        this._auth = _auth;
        this.previous = [];
        this.searchText = '';
        this.maxVisibleItems = 25;
        this.loading = false;
        this.elements = [];
        this._auth.getUserInfo().subscribe(function (value) {
            _this.currentUser = value;
        });
    }
    TableUsersComponent.prototype.oninput = function () {
        this.mdbTablePagination.searchText = this.searchText;
    };
    TableUsersComponent.prototype._initElements = function () {
        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
    };
    TableUsersComponent.prototype.ngOnInit = function () {
        this.refreshData();
    };
    TableUsersComponent.prototype.refreshData = function () {
        var _this = this;
        this.loading = true;
        this.searchText = "";
        var usersSub = this._usersService.getUsers().subscribe(function (res) {
            usersSub.unsubscribe();
            res.results.forEach(function (u) {
                u.lastLogin = u.lastLogin || u.created;
            });
            _this.elements = res.results;
            _this.loading = false;
            _this._initElements();
        });
    };
    TableUsersComponent.prototype.ngAfterViewInit = function () {
        this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);
        this.mdbTablePagination.calculateFirstItemIndex();
        this.mdbTablePagination.calculateLastItemIndex();
        this.cdRef.detectChanges();
    };
    TableUsersComponent.prototype.searchItems = function () {
        var _this = this;
        var prev = this.mdbTable.getDataSource();
        if (!this.searchText) {
            this.mdbTable.setDataSource(this.previous);
            this.elements = this.mdbTable.getDataSource();
        }
        if (this.searchText) {
            this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
            this.mdbTable.setDataSource(prev);
        }
        this.mdbTablePagination.calculateFirstItemIndex();
        this.mdbTablePagination.calculateLastItemIndex();
        this.mdbTable.searchDataObservable(this.searchText).subscribe(function () {
            _this.mdbTablePagination.calculateFirstItemIndex();
            _this.mdbTablePagination.calculateLastItemIndex();
        });
    };
    TableUsersComponent.prototype.FieldsChange = function (user, value) {
        var patchData = { "role": value.checked ? "admin" : "user" };
        this._usersService.updateUser(user._id, patchData).subscribe(function (res1) {
            user.role = res1.role;
        });
    };
    __decorate([
        ViewChild(MdbTablePaginationComponent, { static: true }),
        __metadata("design:type", MdbTablePaginationComponent)
    ], TableUsersComponent.prototype, "mdbTablePagination", void 0);
    __decorate([
        ViewChild(MdbTableDirective, { static: true }),
        __metadata("design:type", MdbTableDirective)
    ], TableUsersComponent.prototype, "mdbTable", void 0);
    __decorate([
        HostListener('input'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TableUsersComponent.prototype, "oninput", null);
    TableUsersComponent = __decorate([
        Component({
            selector: 'table-users',
            templateUrl: './table-users.component.html',
            styleUrls: ['./table-users.component.scss']
        }),
        __metadata("design:paramtypes", [ChangeDetectorRef, UsersService, AuthGuard])
    ], TableUsersComponent);
    return TableUsersComponent;
}());
export { TableUsersComponent };
//# sourceMappingURL=table-users.component.js.map