var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { MdbTablePaginationComponent, MdbTableDirective, MDBModalService } from 'angular-bootstrap-md';
import { Component, ViewChild, HostListener, ChangeDetectorRef } from '@angular/core';
import { ScenariosService } from '../services/scenarios.service';
import { ScenarioModalComponent } from '../alert/edit-scenario.component';
var TableScenariosComponent = /** @class */ (function () {
    function TableScenariosComponent(modalService, cdRef, _scenariosService) {
        this.modalService = modalService;
        this.cdRef = cdRef;
        this._scenariosService = _scenariosService;
        this.previous = [];
        this.searchText = '';
        this.maxVisibleItems = 25;
        this.elements = [];
    }
    TableScenariosComponent.prototype.oninput = function () {
        this.mdbTablePagination.searchText = this.searchText;
    };
    TableScenariosComponent.prototype._initElements = function () {
        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
    };
    TableScenariosComponent.prototype._refresh = function () {
        var _this = this;
        var scenariosSub = this._scenariosService.getScenariosAll().subscribe(function (res) {
            scenariosSub.unsubscribe();
            _this.scenarios = res.results;
            _this.elements = res.results;
            _this._initElements();
        });
    };
    TableScenariosComponent.prototype.ngOnInit = function () {
        this._refresh();
    };
    TableScenariosComponent.prototype.ngAfterViewInit = function () {
        this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);
        this.mdbTablePagination.calculateFirstItemIndex();
        this.mdbTablePagination.calculateLastItemIndex();
        this.cdRef.detectChanges();
    };
    TableScenariosComponent.prototype.searchItems = function () {
        var _this = this;
        var prev = this.mdbTable.getDataSource();
        if (!this.searchText) {
            this.mdbTable.setDataSource(this.previous);
            this.elements = this.mdbTable.getDataSource();
        }
        if (this.searchText) {
            this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
            this.mdbTable.setDataSource(prev);
        }
        this.mdbTablePagination.calculateFirstItemIndex();
        this.mdbTablePagination.calculateLastItemIndex();
        this.mdbTable.searchDataObservable(this.searchText).subscribe(function () {
            _this.mdbTablePagination.calculateFirstItemIndex();
            _this.mdbTablePagination.calculateLastItemIndex();
        });
    };
    TableScenariosComponent.prototype.changeValue = function (scenario, property, event) {
        this.editField = event.target.textContent;
        //console.log("changeValue editField", id, this.editField);
    };
    TableScenariosComponent.prototype.updateList = function (scenario, property, event) {
        var _this = this;
        this.editField = event.target.textContent;
        var patch = {};
        patch[property] = this.editField.trim();
        if (!patch[property] || patch[property] === "" || patch[property] === scenario[property]) {
            return;
        }
        this._scenariosService.updateScenario(scenario._id.toString(), patch).subscribe(function (res) {
            console.log("done", res);
            _this._refresh();
        });
    };
    TableScenariosComponent.prototype.updateJson = function (scenario, property, event) {
        var _this = this;
        this.editField = event.target.textContent.trim();
        try {
            var patch = {};
            var value = JSON.parse(this.editField);
            if (JSON.stringify(value) === JSON.stringify(scenario[property])) {
                return;
            }
            patch[property] = value;
            console.log("editField", patch);
            this._scenariosService.updateScenario(scenario._id.toString(), patch).subscribe(function (res) {
                console.log("done", res);
                _this._refresh();
            });
        }
        catch (e) {
            console.log("error json", e);
            this._refresh();
        }
    };
    TableScenariosComponent.prototype.FieldsChange = function (scenario, property, value) {
        var _this = this;
        var patch = {};
        patch[property] = value.checked;
        this._scenariosService.updateScenario(scenario._id.toString(), patch).subscribe(function (res) {
            console.log("done", res);
            _this._refresh();
        });
    };
    TableScenariosComponent.prototype.openNewScenarioModal = function (scenario) {
        var _this = this;
        var modalRef = this.modalService.show(ScenarioModalComponent, {
            class: 'modal-lg modal-notify',
            containerClass: '',
            data: {
                scenario: scenario
            }
        });
        var sub = modalRef.content.action.subscribe(function (data) {
            sub.unsubscribe();
            console.log("new scenario data", data);
            _this._refresh();
        });
    };
    __decorate([
        ViewChild(MdbTablePaginationComponent, { static: true }),
        __metadata("design:type", MdbTablePaginationComponent)
    ], TableScenariosComponent.prototype, "mdbTablePagination", void 0);
    __decorate([
        ViewChild(MdbTableDirective, { static: true }),
        __metadata("design:type", MdbTableDirective)
    ], TableScenariosComponent.prototype, "mdbTable", void 0);
    __decorate([
        HostListener('input'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TableScenariosComponent.prototype, "oninput", null);
    TableScenariosComponent = __decorate([
        Component({
            selector: 'table-scenarios',
            templateUrl: './table-scenarios.component.html',
            styleUrls: ['./table-scenarios.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalService, ChangeDetectorRef, ScenariosService])
    ], TableScenariosComponent);
    return TableScenariosComponent;
}());
export { TableScenariosComponent };
//# sourceMappingURL=table-scenarios.component.js.map