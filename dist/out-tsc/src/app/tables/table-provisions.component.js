var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, HostListener, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent, MDBModalService } from 'angular-bootstrap-md';
import { ProvisionsService } from '../services/provisions.service';
import { AlertService } from '../services/alert.service';
import { ModalInfoComponent } from '../alert/modalinfo.component';
import { ModalConfirmComponent } from '../alert/confirm.component';
import { ScenariosService } from '../services/scenarios.service';
var TableProvisionsAdminComponent = /** @class */ (function () {
    function TableProvisionsAdminComponent(modalService, _scenariosService, _alertService, cdRef, _provisionsService) {
        this.modalService = modalService;
        this._scenariosService = _scenariosService;
        this._alertService = _alertService;
        this.cdRef = cdRef;
        this._provisionsService = _provisionsService;
        this.filter = {
            showDestroyed: false
        };
        this.filterParams = {
            isDestroyed: false
        };
        this.loading = false;
        this.pagingIsDisabled = false;
        //@ViewChild('row', { static: true }) row: ElementRef;
        this.elements = [];
        this.searchText = '';
        this.selectedprov = null;
        this.showInfo = false;
        this.logShow = false;
        this.logstype = 'provision';
        this.maxVisibleItems = 25;
    }
    TableProvisionsAdminComponent.prototype.oninput = function () {
        this.mdbTablePagination.searchText = this.searchText;
    };
    TableProvisionsAdminComponent.prototype._initElements = function () {
        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
    };
    TableProvisionsAdminComponent.prototype._process = function (provisions) {
        var _this = this;
        provisions.forEach(function (p) {
            p._scenario = _this.scenarios.filter(function (s) { return s.name === p.scenario; });
            _this._provisionsService.timeRunning(p);
        });
        if (this.elements.length === 0) {
            this.elements = provisions;
        }
        else {
            this.elements.forEach(function (p, index, object) {
                var found = provisions.filter(function (a) { return a._id.toString() === p._id.toString(); });
                if (found.length) {
                    p.status = found[0].status;
                    p.statusVms = found[0].statusVms;
                    p.isDestroyed = found[0].isDestroyed;
                    p.outputs = found[0].outputs;
                    p.destroy = found[0].destroy;
                    this._provisionsService.timeRunning(p);
                }
                else {
                    object.splice(index, 1);
                }
            }.bind(this));
            provisions.forEach(function (p) {
                var found = this.elements.filter(function (a) { return a._id.toString() === p._id.toString(); });
                if (found.length === 0) {
                    this.elements.unshift(p);
                }
            }.bind(this));
        }
        this._initElements();
    };
    TableProvisionsAdminComponent.prototype.ngOnInit = function () {
        var _this = this;
        var scenariosSub = this._scenariosService.getScenariosAll().subscribe(function (res) {
            scenariosSub.unsubscribe();
            _this.scenarios = res.results;
            /*this.subscription = timer(0, 8000).pipe( switchMap(() => this._provisionsService.getProvisionsAdmin(this.filterParams) ) ).subscribe(provisions => {
              this._process(provisions.results);
            });*/
            _this.refreshData();
        });
        //this._initElements();
    };
    TableProvisionsAdminComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    TableProvisionsAdminComponent.prototype.ngAfterViewInit = function () {
        if (this.mdbTablePagination) {
            this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);
            this.mdbTablePagination.calculateFirstItemIndex();
            this.mdbTablePagination.calculateLastItemIndex();
            this.cdRef.detectChanges();
        }
    };
    TableProvisionsAdminComponent.prototype.searchItems = function () {
        var _this = this;
        var prev = this.mdbTable.getDataSource();
        if (!this.searchText) {
            this.mdbTable.setDataSource(this.previous);
            this.elements = this.mdbTable.getDataSource();
        }
        if (this.searchText) {
            this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
            this.mdbTable.setDataSource(prev);
        }
        if (this.mdbTablePagination) {
            this.mdbTablePagination.calculateFirstItemIndex();
            this.mdbTablePagination.calculateLastItemIndex();
            this.mdbTable.searchDataObservable(this.searchText).subscribe(function () {
                _this.mdbTablePagination.calculateFirstItemIndex();
                _this.mdbTablePagination.calculateLastItemIndex();
            });
        }
    };
    TableProvisionsAdminComponent.prototype.showLogs = function ($event, provision, type) {
        $event.preventDefault();
        $event.stopPropagation();
        this.logstype = type;
        this.logShow = false;
        this.selectedprov = provision;
        this.logShow = true;
    };
    TableProvisionsAdminComponent.prototype.onLogsClose = function () {
        this.selectedprov = null;
        this.logShow = false;
    };
    TableProvisionsAdminComponent.prototype.openConfirmStartModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-info',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm Start VMs?',
                    icon: 'play',
                    buttonColor: 'grey'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._startVms(provision);
        });
    };
    TableProvisionsAdminComponent.prototype._startVms = function (provision) {
        var _this = this;
        var sub = this._provisionsService.startVms(provision._id.toString(), provision.user._id).subscribe(function (res) {
            provision.startVms = res.startVms;
            sub.unsubscribe();
            _this._alertService.showAlert({
                type: 'alert-primary',
                text: "Starting all VMs for scenario '" + provision.scenario + "'..."
            });
        });
    };
    TableProvisionsAdminComponent.prototype.openConfirmStopModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-info',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm Stop VMs?',
                    icon: 'stop',
                    buttonColor: 'grey'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._stopVms(provision);
        });
    };
    TableProvisionsAdminComponent.prototype._stopVms = function (provision) {
        var _this = this;
        var sub = this._provisionsService.stopVms(provision._id.toString(), provision.user._id).subscribe(function (res) {
            provision.startVms = res.startVms;
            sub.unsubscribe();
            _this._alertService.showAlert({
                type: 'alert-primary',
                text: "Stopping all VMs for scenario '" + provision.scenario + "'..."
            });
        });
    };
    TableProvisionsAdminComponent.prototype.openInfoModal = function (provision) {
        this.modalService.show(ModalInfoComponent, {
            backdrop: true,
            keyboard: true,
            focus: true,
            show: false,
            ignoreBackdropClick: false,
            class: 'modal-lg',
            containerClass: '',
            animated: true,
            data: {
                info: provision
            }
        });
    };
    TableProvisionsAdminComponent.prototype.openConfirmDestroyModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-danger',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm destroy this provision?',
                    icon: 'times-circle'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.newDestroy(provision._id.toString(), provision.user._id).subscribe(function (provUpdated) {
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Provision of scenario '" + provision.scenario + "' is going to be destroyed"
                });
                provision.destroy = provUpdated.destroy;
            });
        });
    };
    TableProvisionsAdminComponent.prototype.openConfirmDeleteModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-danger',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm delete?',
                    icon: 'trash-alt'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.delProvision(provision._id, provision.user._id).subscribe(function (res) {
                _this.elements = _this.elements.filter(function (e) {
                    return e._id.toString() !== provision._id.toString();
                });
                _this._initElements();
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Provision entry '" + provision.scenario + "' was deleted"
                });
            });
        });
    };
    TableProvisionsAdminComponent.prototype.refreshData = function () {
        var _this = this;
        this.elements = [];
        this.loading = true;
        this.searchText = "";
        var instantSubs = this._provisionsService.getProvisionsAdmin(this.filterParams).subscribe(function (provisions) {
            instantSubs.unsubscribe();
            _this.loading = false;
            _this._process(provisions.results);
        });
    };
    TableProvisionsAdminComponent.prototype.onCheckValue = function () {
        this.filterParams = {};
        if (!this.filter.showDestroyed) {
            this.filterParams.isDestroyed = false;
        }
        this.refreshData();
    };
    TableProvisionsAdminComponent.prototype.openConfirmExtendModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-info',
            containerClass: '',
            data: {
                info: {
                    title: "Extend running VMs for " + this._provisionsService.RUNNING_PERIOD + " days?",
                    icon: 'plus-square',
                    buttonColor: 'grey'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.extend(provision._id.toString(), provision.user._id).subscribe(function (res) {
                provision.countExtend = res.countExtend;
                provision.timeRunning = res.timeRunning;
                provision.runningFrom = res.runningFrom;
                _this._provisionsService.timeRunning(provision);
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Running period extended another " + _this._provisionsService.RUNNING_PERIOD + " days (from now) for provision '" + provision.scenario + "'"
                });
            });
        });
    };
    __decorate([
        ViewChild(MdbTablePaginationComponent, { static: true }),
        __metadata("design:type", MdbTablePaginationComponent)
    ], TableProvisionsAdminComponent.prototype, "mdbTablePagination", void 0);
    __decorate([
        ViewChild(MdbTableDirective, { static: true }),
        __metadata("design:type", MdbTableDirective)
    ], TableProvisionsAdminComponent.prototype, "mdbTable", void 0);
    __decorate([
        HostListener('input'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], TableProvisionsAdminComponent.prototype, "oninput", null);
    TableProvisionsAdminComponent = __decorate([
        Component({
            selector: 'table-provisions',
            templateUrl: './table-provisions.component.html',
            styleUrls: ['./table-provisions.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalService, ScenariosService, AlertService, ChangeDetectorRef, ProvisionsService])
    ], TableProvisionsAdminComponent);
    return TableProvisionsAdminComponent;
}());
export { TableProvisionsAdminComponent };
//# sourceMappingURL=table-provisions.component.js.map