var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Output, EventEmitter } from '@angular/core';
import { ProvisionsService } from '../services/provisions.service';
import { ScenariosService } from '../services/scenarios.service';
import { AuthGuard } from '../services/auth.guard';
import { NewProvisionConfirmComponent } from '../alert/new-provision.component';
import { MDBModalService } from 'angular-bootstrap-md';
var ScenariosComponent = /** @class */ (function () {
    function ScenariosComponent(modalService, _provisionsService, _scenariosService, _auth) {
        var _this = this;
        this.modalService = modalService;
        this._provisionsService = _provisionsService;
        this._scenariosService = _scenariosService;
        this._auth = _auth;
        this.onStartProvision = new EventEmitter();
        this._auth.getUserInfo().subscribe(function (value) {
            _this.user = value;
        });
    }
    ScenariosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.scenariosSub = this._scenariosService.getScenarios().subscribe(function (res) {
            _this.scenarios = res.results;
            _this.scenariosSub.unsubscribe();
            console.log("scenarios", _this.scenarios);
        });
    };
    ScenariosComponent.prototype.ngOnDestroy = function () { };
    ScenariosComponent.prototype.openNewProvisionConfirmModal = function (scenario) {
        var _this = this;
        var modalRef = this.modalService.show(NewProvisionConfirmComponent, {
            class: 'modal-md modal-notify',
            containerClass: '',
            data: {
                scenario: scenario
            }
        });
        var sub = modalRef.content.action.subscribe(function (data) {
            sub.unsubscribe();
            var postData = {
                scenario: scenario.name,
                description: data.description,
                isExternalAccess: data.isExternalAccess
            };
            if (data.servers) {
                postData["vmImage"] = data.servers;
            }
            _this._provisionsService.newProvision(postData, _this.user._id).subscribe(function (res) {
                console.log("Done!", res);
                _this.onStartProvision.emit(scenario);
            });
        });
    };
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], ScenariosComponent.prototype, "onStartProvision", void 0);
    ScenariosComponent = __decorate([
        Component({
            selector: 'app-scenarios',
            templateUrl: './scenarios.component.html',
            styleUrls: ['./scenarios.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalService, ProvisionsService, ScenariosService, AuthGuard])
    ], ScenariosComponent);
    return ScenariosComponent;
}());
export { ScenariosComponent };
//# sourceMappingURL=scenarios.component.js.map