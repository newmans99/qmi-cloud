var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
var ModalInfoComponent = /** @class */ (function () {
    function ModalInfoComponent(modalRef) {
        this.modalRef = modalRef;
    }
    ModalInfoComponent.prototype.ngOnInit = function () {
    };
    ModalInfoComponent.prototype.ngOnDestroy = function () {
    };
    ModalInfoComponent = __decorate([
        Component({
            selector: 'qmi-modalinfo',
            templateUrl: './modalinfo.component.html',
            styleUrls: ['./modalinfo.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalRef])
    ], ModalInfoComponent);
    return ModalInfoComponent;
}());
export { ModalInfoComponent };
//# sourceMappingURL=modalinfo.component.js.map