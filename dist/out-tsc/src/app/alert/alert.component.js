var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild, ElementRef } from '@angular/core';
import { AlertService } from '../services/alert.service';
var AlertComponent = /** @class */ (function () {
    function AlertComponent(_alertService) {
        this._alertService = _alertService;
        this.alert = null;
    }
    AlertComponent.prototype.ngOnInit = function () {
        this.subscription = this._alertService.getAlertEmitter().subscribe(function (data) {
            this.alert = data;
        }.bind(this));
    };
    AlertComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    AlertComponent.prototype.closeAlert = function () {
        this.alert = null;
    };
    __decorate([
        ViewChild('qmialert', { static: true }),
        __metadata("design:type", ElementRef)
    ], AlertComponent.prototype, "alertEl", void 0);
    AlertComponent = __decorate([
        Component({
            selector: 'qmi-alert',
            templateUrl: './alert.component.html',
            styleUrls: ['./alert.component.scss']
        }),
        __metadata("design:paramtypes", [AlertService])
    ], AlertComponent);
    return AlertComponent;
}());
export { AlertComponent };
//# sourceMappingURL=alert.component.js.map