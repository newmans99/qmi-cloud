var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';
import { ScenariosService } from '../services/scenarios.service';
import { SubscriptionsService } from '../services/subscriptions.service';
var ScenarioModalComponent = /** @class */ (function () {
    function ScenarioModalComponent(modalRef, _scenariosService, _subscriptionsService) {
        this.modalRef = modalRef;
        this._scenariosService = _scenariosService;
        this._subscriptionsService = _subscriptionsService;
        this.action = new Subject();
        this.sendData = {
            availableProductVersions: [{
                    product: 'String: <ie: Qlik Sense>',
                    vmTypeDefault: 'String: <i.e: Standard_D8s_v3>',
                    diskSizeGbDefault: 'Integer: <values: 128,250,500,750,1000>',
                    index: 'vm1',
                    versions: []
                }]
        };
    }
    ScenarioModalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._subscriptionsService.getSubscriptions().subscribe(function (res) {
            _this.subscriptions = res.results;
            if (_this.scenario) {
                _this.sendData = JSON.parse(JSON.stringify(_this.scenario));
            }
            if (!_this.sendData.subscription) {
                _this.sendData.subscription = res.results[0]._id;
            }
        });
    };
    ScenarioModalComponent.prototype.ngOnDestroy = function () {
    };
    ScenarioModalComponent.prototype.confirm = function () {
        var _this = this;
        console.log("sendData", this.sendData);
        //this.action.next(this.sendData);
        if (this.sendData._id) {
            var id = this.sendData._id.toString();
            this.sendData._id = undefined;
            this._scenariosService.updateScenario(id, this.sendData).subscribe(function (res) {
                console.log("done", res);
                _this.action.next("DONE!!!");
                _this.modalRef.hide();
            });
        }
        else {
            this._scenariosService.addScenario(this.sendData).subscribe(function (res) {
                console.log("done", res);
                _this.action.next("DONE!!!");
                _this.modalRef.hide();
            });
        }
    };
    ScenarioModalComponent.prototype.delete = function () {
        var _this = this;
        this._scenariosService.deleteScenario(this.sendData._id).subscribe(function (res) {
            console.log("done", res);
            _this.action.next("DONE!!!");
            _this.modalRef.hide();
        });
    };
    ScenarioModalComponent.prototype.checkOnchange = function ($event, field) {
        console.log("Checked?", $event.checked);
        this.sendData[field] = $event.checked;
    };
    ScenarioModalComponent.prototype.updateJson = function (event, property) {
        var editField = event.target.textContent.trim();
        try {
            var value = JSON.parse(editField);
            this.sendData[property] = value;
        }
        catch (e) {
            console.log("error json", e);
        }
    };
    ScenarioModalComponent = __decorate([
        Component({
            selector: 'qmi-new-scenario',
            templateUrl: './edit-scenario.component.html',
            styleUrls: ['./edit-scenario.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalRef, ScenariosService, SubscriptionsService])
    ], ScenarioModalComponent);
    return ScenarioModalComponent;
}());
export { ScenarioModalComponent };
//# sourceMappingURL=edit-scenario.component.js.map