var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';
import { SubscriptionsService } from '../services/subscriptions.service';
var SubscriptionModalComponent = /** @class */ (function () {
    function SubscriptionModalComponent(modalRef, _subscriptionsService) {
        this.modalRef = modalRef;
        this._subscriptionsService = _subscriptionsService;
        this.action = new Subject();
        this.sendData = {};
    }
    SubscriptionModalComponent.prototype.ngOnInit = function () {
        if (this.subscription) {
            this.sendData = JSON.parse(JSON.stringify(this.subscription));
        }
    };
    SubscriptionModalComponent.prototype.ngOnDestroy = function () {
    };
    SubscriptionModalComponent.prototype.confirm = function () {
        var _this = this;
        console.log("sendData", this.sendData);
        //this.action.next(this.sendData);
        if (this.sendData._id) {
            var id = this.sendData._id.toString();
            this.sendData._id = undefined;
            this._subscriptionsService.updateSubscription(id, this.sendData).subscribe(function (res) {
                console.log("done", res);
                _this.action.next("DONE!!!");
                _this.modalRef.hide();
            });
        }
        else {
            this._subscriptionsService.addSubscription(this.sendData).subscribe(function (res) {
                console.log("done", res);
                _this.action.next("DONE!!!");
                _this.modalRef.hide();
            });
        }
    };
    SubscriptionModalComponent.prototype.delete = function () {
        var _this = this;
        this._subscriptionsService.deleteScenario(this.sendData._id).subscribe(function (res) {
            console.log("done", res);
            _this.action.next("DONE!!!");
            _this.modalRef.hide();
        });
    };
    SubscriptionModalComponent = __decorate([
        Component({
            selector: 'qmi-new-subscription',
            templateUrl: './edit-subscription.component.html',
            styleUrls: ['./edit-subscription.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalRef, SubscriptionsService])
    ], SubscriptionModalComponent);
    return SubscriptionModalComponent;
}());
export { SubscriptionModalComponent };
//# sourceMappingURL=edit-subscription.component.js.map