var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';
import { ScenariosService } from '../services/scenarios.service';
var NewProvisionConfirmComponent = /** @class */ (function () {
    function NewProvisionConfirmComponent(modalRef, _scenariosService) {
        this.modalRef = modalRef;
        this._scenariosService = _scenariosService;
        this.action = new Subject();
        this.sendData = {
            description: "",
            servers: null,
            isExternalAccess: false,
        };
        this.selectedProductVersion = {};
        this.selectedVmType = {};
        this.selectedNodeCount = {};
        this.selectedDiskSizeGb = {};
        this.servers = {};
    }
    NewProvisionConfirmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.vmTypesSub = this._scenariosService.getScenarioVmtypes().subscribe(function (res) {
            _this.vmTypes = res.results;
            if (_this.scenario.availableProductVersions.length) {
                _this.scenario.availableProductVersions.forEach(function (server) {
                    if (server.vmTypeDefault) {
                        _this.selectedVmType[server.index] = server.vmTypeDefault;
                    }
                    if (server.nodeCount) {
                        _this.selectedNodeCount[server.index] = server.nodeCount;
                    }
                    _this.selectedDiskSizeGb[server.index] = server.diskSizeGbDefault || 500;
                    if (server.versions && server.versions.length) {
                        var lastIndex = server.versions.length - 1;
                        _this.selectedProductVersion[server.index] = server.productVersionDefault ? server.productVersionDefault : server.versions[lastIndex].name;
                    }
                });
            }
            _this.vmTypesSub.unsubscribe();
        });
    };
    NewProvisionConfirmComponent.prototype.ngOnDestroy = function () {
    };
    NewProvisionConfirmComponent.prototype.confirm = function () {
        var _this = this;
        if (!this.sendData.description || this.sendData.description.trim() === "") {
            return;
        }
        this.sendData.servers = {};
        var _loop_1 = function (key) {
            if (!this_1.sendData.servers[key]) {
                this_1.sendData.servers[key] = {};
            }
            if (this_1.selectedVmType[key]) {
                this_1.sendData.servers[key].vmType = this_1.selectedVmType[key];
            }
            if (this_1.selectedNodeCount[key]) {
                this_1.sendData.servers[key].nodeCount = this_1.selectedNodeCount[key];
            }
            if (this_1.selectedDiskSizeGb[key]) {
                this_1.sendData.servers[key].diskSizeGb = this_1.selectedDiskSizeGb[key];
            }
            this_1.scenario.availableProductVersions.forEach(function (server) {
                server.versions.forEach(function (v) {
                    if (v.name === _this.selectedProductVersion[key]) {
                        _this.sendData.servers[key].version = v;
                    }
                });
            });
        };
        var this_1 = this;
        for (var key in this.selectedVmType) {
            _loop_1(key);
        }
        console.log("sendData", this.sendData);
        this.action.next(this.sendData);
        this.modalRef.hide();
    };
    NewProvisionConfirmComponent.prototype.checkOnchange = function ($event) {
        console.log("Checked?", $event.checked);
        this.sendData.isExternalAccess = $event.checked;
    };
    NewProvisionConfirmComponent = __decorate([
        Component({
            selector: 'qmi-new-provision',
            templateUrl: './new-provision.component.html',
            styleUrls: ['./new-provision.component.scss']
        }),
        __metadata("design:paramtypes", [MDBModalRef, ScenariosService])
    ], NewProvisionConfirmComponent);
    return NewProvisionConfirmComponent;
}());
export { NewProvisionConfirmComponent };
//# sourceMappingURL=new-provision.component.js.map