var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthGuard } from '../services/auth.guard';
var MyHttpInterceptor = /** @class */ (function () {
    function MyHttpInterceptor(router, _auth) {
        this.router = router;
        this._auth = _auth;
    }
    MyHttpInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request).pipe(tap(function () { }, function (err) {
            if (err instanceof HttpErrorResponse) {
                if (err.status !== 401) {
                    return;
                }
                console.log("Interceptor error 401!!");
                _this._auth.clearUser();
                _this.router.navigate(['home']);
            }
        }));
    };
    MyHttpInterceptor = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Router, AuthGuard])
    ], MyHttpInterceptor);
    return MyHttpInterceptor;
}());
export { MyHttpInterceptor };
//# sourceMappingURL=http.interceptor.js.map