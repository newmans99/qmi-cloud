var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { HomeComponent } from './home/home.component';
import { ProvisionsComponent } from './provisions/provisions.component';
import { AuthGuard } from './services/auth.guard';
import { ProvisionsService } from './services/provisions.service';
import { ScenariosService } from './services/scenarios.service';
import { UsersService } from './services/users.service';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MarkdownModule, MarkedRenderer } from 'ngx-markdown';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LogsComponent } from './logs/logs.component';
import { ScenariosComponent } from './scenarios/scenarios.component';
import { AdminComponent } from './admin/admin.component';
import { PopoverconfirmComponent } from './popoverconfirm/popoverconfirm.component';
import { FormsModule } from '@angular/forms';
import { MyHttpInterceptor } from './interceptors/http.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TableProvisionsAdminComponent } from './tables/table-provisions.component';
import { TableScenariosComponent } from './tables/table-scenarios.component';
import { TableUsersComponent } from './tables/table-users.component';
import { TableNotificationsComponent } from './tables/table-notifications.component';
import { AlertComponent } from './alert/alert.component';
import { AlertService } from './services/alert.service';
import { ModalInfoComponent } from './alert/modalinfo.component';
import { ModalConfirmComponent } from './alert/confirm.component';
import { FilterPipe } from './filter.pipe';
import { FaqComponent } from './faq/faq.component';
import { NewProvisionConfirmComponent } from './alert/new-provision.component';
import { ScenarioModalComponent } from './alert/edit-scenario.component';
import { SubscriptionModalComponent } from './alert/edit-subscription.component';
import { TableSubsComponent } from './tables/table-subs.component';
import { SubscriptionsService } from './services/subscriptions.service';
export function markedOptions() {
    var renderer = new MarkedRenderer();
    renderer.blockquote = function (text) {
        return '<blockquote class="blockquote"><p>' + text + '</p></blockquote>';
    };
    return { renderer: renderer };
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                HomeComponent,
                ProvisionsComponent,
                LogsComponent,
                ScenariosComponent,
                AdminComponent,
                PopoverconfirmComponent,
                TableProvisionsAdminComponent,
                TableUsersComponent,
                AlertComponent,
                ModalInfoComponent,
                ModalConfirmComponent,
                FilterPipe,
                FaqComponent,
                NewProvisionConfirmComponent,
                TableScenariosComponent,
                TableNotificationsComponent,
                ScenarioModalComponent,
                SubscriptionModalComponent,
                TableSubsComponent
            ],
            imports: [
                BrowserModule,
                AppRoutingModule,
                UiModule,
                HttpClientModule,
                FormsModule,
                MDBBootstrapModule.forRoot(),
                MarkdownModule.forRoot({
                    loader: HttpClient
                }),
            ],
            providers: [
                { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true },
                ProvisionsService,
                ScenariosService,
                SubscriptionsService,
                UsersService,
                AlertService,
                AuthGuard
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map