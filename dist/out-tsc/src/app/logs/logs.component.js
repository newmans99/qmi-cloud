var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter, ViewChild, HostListener } from '@angular/core';
import { ProvisionsService } from '../services/provisions.service';
import { timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
var LogsComponent = /** @class */ (function () {
    function LogsComponent(_provisionsService) {
        this._provisionsService = _provisionsService;
        this.content = null;
        this.onClose = new EventEmitter();
    }
    LogsComponent.prototype.onClick = function (targetElement) {
        var clickedInside = this.insideElement.nativeElement.contains(targetElement);
        if (!clickedInside) {
            this.onClose.emit(false);
        }
    };
    LogsComponent.prototype.refresh = function () {
        var _this = this;
        if (!this.content) {
            if (this.type === "provision") {
                this.sub = timer(0, 5000).pipe(switchMap(function () { return _this._provisionsService.getProvisionLogs(_this.selectedprov._id); })).subscribe(function (content) {
                    _this.content = content;
                });
            }
            else if (this.type === "destroy") {
                this.sub = timer(0, 5000).pipe(switchMap(function () { return _this._provisionsService.getDestroyLogs(_this.selectedprov.destroy._id); })).subscribe(function (content) {
                    _this.content = content;
                });
            }
        }
    };
    LogsComponent.prototype.ngOnInit = function () { };
    LogsComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        this.content = null;
        if (this.sub) {
            this.sub.unsubscribe();
            this.sub = null;
        }
        if (changes.show && changes.show.currentValue) {
            if (this.type === "provision") {
                this.sub = timer(0, 5000).pipe(switchMap(function () { return _this._provisionsService.getProvisionLogs(_this.selectedprov._id); })).subscribe(function (content) {
                    _this.content = content;
                });
            }
            else if (this.type === "destroy") {
                this.sub = timer(0, 5000).pipe(switchMap(function () { return _this._provisionsService.getDestroyLogs(_this.selectedprov.destroy._id); })).subscribe(function (content) {
                    _this.content = content;
                });
            }
        }
    };
    LogsComponent.prototype.ngOnDestroy = function () {
        if (this.sub) {
            this.sub.unsubscribe();
            this.sub = null;
        }
    };
    LogsComponent.prototype.close = function () {
        this.content = null;
        if (this.sub) {
            this.sub.unsubscribe();
            this.sub = null;
        }
        this.onClose.emit(false);
    };
    __decorate([
        ViewChild("insideElement"),
        __metadata("design:type", Object)
    ], LogsComponent.prototype, "insideElement", void 0);
    __decorate([
        HostListener('document:click', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], LogsComponent.prototype, "onClick", null);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], LogsComponent.prototype, "show", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], LogsComponent.prototype, "selectedprov", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], LogsComponent.prototype, "type", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], LogsComponent.prototype, "onClose", void 0);
    LogsComponent = __decorate([
        Component({
            selector: 'app-logs',
            templateUrl: './logs.component.html',
            styleUrls: ['./logs.component.scss']
        }),
        __metadata("design:paramtypes", [ProvisionsService])
    ], LogsComponent);
    return LogsComponent;
}());
export { LogsComponent };
//# sourceMappingURL=logs.component.js.map