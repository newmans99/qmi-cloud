var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { ProvisionsService } from '../services/provisions.service';
import { timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthGuard } from '../services/auth.guard';
import { ScenariosService } from '../services/scenarios.service';
import { AlertService } from '../services/alert.service';
import { MDBModalService } from 'angular-bootstrap-md';
import { ModalInfoComponent } from '../alert/modalinfo.component';
import { ModalConfirmComponent } from '../alert/confirm.component';
var ProvisionsComponent = /** @class */ (function () {
    function ProvisionsComponent(modalService, _alertService, _provisionsService, _scenariosService, _auth) {
        var _this = this;
        this.modalService = modalService;
        this._alertService = _alertService;
        this._provisionsService = _provisionsService;
        this._scenariosService = _scenariosService;
        this._auth = _auth;
        this.logShow = false;
        this.logstype = 'provision';
        this.selectedprov = null;
        this._auth.getUserInfo().subscribe(function (value) {
            _this._userId = value ? value._id : null;
        });
    }
    ProvisionsComponent.prototype._refresh = function () {
        var _this = this;
        this.instantSubs = this._provisionsService.getProvisionsByUser(this._userId).subscribe(function (provisions) {
            provisions = provisions.results;
            provisions.forEach(function (p) {
                p._scenario = _this.scenarios.filter(function (s) { return s.name === p.scenario; });
                _this._provisionsService.timeRunning(p);
            });
            _this.provisions = provisions.filter(function (p) { return !p.destroy || !p.destroy.status || p.destroy.status !== 'destroyed'; });
            _this.destroys = provisions.filter(function (p) { return p.destroy && p.destroy.status === 'destroyed'; });
            _this.instantSubs.unsubscribe();
        });
    };
    ProvisionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.scenariosSub = this._scenariosService.getScenariosAll().subscribe(function (res) {
            _this.scenarios = res.results;
            _this.scenariosSub.unsubscribe();
            _this.subscription = timer(0, 8000).pipe(switchMap(function () { return _this._provisionsService.getProvisionsByUser(_this._userId); })).subscribe(function (provisions) {
                provisions = provisions.results;
                provisions.forEach(function (p) {
                    p._scenario = _this.scenarios.filter(function (s) { return s.name === p.scenario; });
                    _this._provisionsService.timeRunning(p);
                });
                _this.provisions = provisions.filter(function (p) { return !p.destroy || !p.destroy.status || p.destroy.status !== 'destroyed'; });
                _this.destroys = provisions.filter(function (p) { return p.destroy && p.destroy.status === 'destroyed'; });
            });
        });
    };
    ProvisionsComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
        if (this.instantSubs) {
            this.instantSubs.unsubscribe();
        }
    };
    ProvisionsComponent.prototype.setModal = function (provision, frame) {
        frame.show();
        this._provisionsService.setSelectedProv(provision);
    };
    ProvisionsComponent.prototype.del = function (provision) {
        var _this = this;
        this._provisionsService.delProvision(provision._id.toString(), this._userId).subscribe(function (res) {
            _this._refresh();
            _this._alertService.showAlert({
                type: 'alert-primary',
                text: "Provision entry '" + provision.scenario + "' was deleted from your history"
            });
        });
    };
    ProvisionsComponent.prototype.openConfirmDestroyModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-danger',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm destroy this provision?',
                    icon: 'times-circle'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.newDestroy(provision._id.toString(), _this._userId).subscribe(function (res) {
                _this._refresh();
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Provision of scenario '" + provision.scenario + "' is going to be destroyed"
                });
            });
        });
    };
    ProvisionsComponent.prototype.openConfirmStopModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-info',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm Stop VMs?',
                    icon: 'stop',
                    buttonColor: 'grey'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.stopVms(provision._id.toString(), _this._userId).subscribe(function (res) {
                provision.statusVms = res.statusVms;
                provision.timeRunning = res.timeRunning;
                provision.runningFrom = res.runningFrom;
                _this._provisionsService.timeRunning(provision);
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Stopping all VMs for scenario '" + provision.scenario + "'..."
                });
            });
        });
    };
    ProvisionsComponent.prototype.openConfirmStartModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-info',
            containerClass: '',
            data: {
                info: {
                    title: 'Confirm Start VMs?',
                    icon: 'play',
                    buttonColor: 'grey'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.startVms(provision._id.toString(), _this._userId).subscribe(function (res) {
                provision.statusVms = res.statusVms;
                provision.timeRunning = res.timeRunning;
                provision.runningFrom = res.runningFrom;
                _this._provisionsService.timeRunning(provision);
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Starting all VMs for scenario '" + provision.scenario + "'..."
                });
            });
        });
    };
    ProvisionsComponent.prototype.openConfirmExtendModal = function (provision) {
        var _this = this;
        var modalRef = this.modalService.show(ModalConfirmComponent, {
            class: 'modal-sm modal-notify modal-info',
            containerClass: '',
            data: {
                info: {
                    title: "Extend running VMs for " + this._provisionsService.RUNNING_PERIOD + " days?",
                    icon: 'plus-square',
                    buttonColor: 'grey'
                }
            }
        });
        var sub = modalRef.content.action.subscribe(function (result) {
            sub.unsubscribe();
            _this._provisionsService.extend(provision._id.toString(), _this._userId).subscribe(function (res) {
                provision.countExtend = res.countExtend;
                provision.timeRunning = res.timeRunning;
                provision.runningFrom = res.runningFrom;
                _this._provisionsService.timeRunning(provision);
                _this._alertService.showAlert({
                    type: 'alert-primary',
                    text: "Running period extended another " + _this._provisionsService.RUNNING_PERIOD + " days (from now) for provision '" + provision.scenario + "'"
                });
            });
        });
    };
    ProvisionsComponent.prototype.showLogs = function ($event, provision, type) {
        $event.preventDefault();
        $event.stopPropagation();
        this.logstype = type;
        this.logShow = false;
        this.selectedprov = provision;
        this.logShow = true;
    };
    ProvisionsComponent.prototype.openModal = function (provision) {
        this.modalService.show(ModalInfoComponent, {
            class: 'modal-lg',
            containerClass: '',
            data: {
                info: provision
            }
        });
    };
    ProvisionsComponent.prototype.onLogsClose = function () {
        this.selectedprov = null;
        this.logShow = false;
    };
    ProvisionsComponent.prototype.onStartProvision = function (scenario) {
        this._alertService.showAlert({
            type: 'alert-primary',
            text: "Scenario '" + scenario.name + "' is going to be provisioned. Scroll up to your Provisions to watch out progress."
        });
        this._refresh();
    };
    ProvisionsComponent = __decorate([
        Component({
            selector: 'app-provisions',
            templateUrl: './provisions.component.html',
            styleUrls: ['./provisions.component.scss'],
            providers: [ProvisionsService]
        }),
        __metadata("design:paramtypes", [MDBModalService, AlertService, ProvisionsService, ScenariosService, AuthGuard])
    ], ProvisionsComponent);
    return ProvisionsComponent;
}());
export { ProvisionsComponent };
//# sourceMappingURL=provisions.component.js.map