var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
var ScenariosService = /** @class */ (function () {
    function ScenariosService(httpClient) {
        this.httpClient = httpClient;
    }
    ScenariosService.prototype.getScenarios = function () {
        return this.httpClient.get(environment.apiVersionPath + "/scenarios");
    };
    ScenariosService.prototype.getScenariosAll = function () {
        return this.httpClient.get(environment.apiVersionPath + "/scenarios/all");
    };
    ScenariosService.prototype.getScenarioVmtypes = function () {
        return this.httpClient.get(environment.apiVersionPath + "/scenarios/vmtypes");
    };
    ScenariosService.prototype.updateScenario = function (id, patchData) {
        return this.httpClient.put(environment.apiVersionPath + "/scenarios/" + id, patchData);
    };
    ScenariosService.prototype.addScenario = function (data) {
        return this.httpClient.post(environment.apiVersionPath + "/scenarios", data);
    };
    ScenariosService.prototype.deleteScenario = function (id) {
        return this.httpClient.delete(environment.apiVersionPath + "/scenarios/" + id);
    };
    ScenariosService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient])
    ], ScenariosService);
    return ScenariosService;
}());
export { ScenariosService };
//# sourceMappingURL=scenarios.service.js.map