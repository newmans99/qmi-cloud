var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
var SubscriptionsService = /** @class */ (function () {
    function SubscriptionsService(httpClient) {
        this.httpClient = httpClient;
    }
    SubscriptionsService.prototype.getSubscriptions = function () {
        return this.httpClient.get(environment.apiVersionPath + "/subscriptions");
    };
    SubscriptionsService.prototype.addSubscription = function (data) {
        return this.httpClient.post(environment.apiVersionPath + "/subscriptions", data);
    };
    SubscriptionsService.prototype.updateSubscription = function (id, patchData) {
        return this.httpClient.put(environment.apiVersionPath + "/subscriptions/" + id, patchData);
    };
    SubscriptionsService.prototype.deleteScenario = function (id) {
        return this.httpClient.delete(environment.apiVersionPath + "/subscriptions/" + id);
    };
    SubscriptionsService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient])
    ], SubscriptionsService);
    return SubscriptionsService;
}());
export { SubscriptionsService };
//# sourceMappingURL=subscriptions.service.js.map