var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import * as moment from 'moment';
var ProvisionsService = /** @class */ (function () {
    function ProvisionsService(httpClient) {
        this.httpClient = httpClient;
        this.RUNNING_PERIOD = 4;
        this.STOP_PERIOD = 10;
    }
    ProvisionsService.prototype.getProvisionsAdmin = function (filter) {
        // Initialize Params Object
        var params = new HttpParams();
        if (filter) {
            params = params.append("filter", JSON.stringify(filter));
        }
        return this.httpClient.get(environment.apiVersionPath + "/provisions", { params: params });
    };
    ProvisionsService.prototype.getProvisionsByUser = function (userId) {
        return this.httpClient.get(environment.apiVersionPath + "/users/" + userId + "/provisions");
    };
    ProvisionsService.prototype.getDestroyProvisionsAdmin = function () {
        return this.httpClient.get(environment.apiVersionPath + "/destroyprovisions");
    };
    ProvisionsService.prototype.newProvision = function (body, userId) {
        return this.httpClient.post(environment.apiVersionPath + "/users/" + userId + "/provisions", body);
    };
    ProvisionsService.prototype.delProvision = function (id, userId) {
        return this.httpClient.delete(environment.apiVersionPath + "/users/" + userId + "/provisions/" + id);
    };
    ProvisionsService.prototype.newDestroy = function (id, userId) {
        return this.httpClient.post(environment.apiVersionPath + "/users/" + userId + "/provisions/" + id + "/destroy", null);
    };
    ProvisionsService.prototype.getDestroyProvisions = function (userId) {
        return this.httpClient.get(environment.apiVersionPath + "/users/" + userId + "/destroyprovisions");
    };
    /*
    getCombinedProvisions(userId): Observable<any> {
      return forkJoin(this.getProvisionsByUser(userId), this.getDestroyProvisions(userId))
    }
  
    getCombinedProvisionsAdmin(): Observable<any> {
      return forkJoin(this.getProvisionsAdmin(), this.getDestroyProvisionsAdmin())
    }*/
    ProvisionsService.prototype.getProvisionLogs = function (id) {
        return this.httpClient.get(environment.apiVersionPath + "/provisions/" + id + "/logs", { responseType: 'text' });
    };
    ProvisionsService.prototype.getDestroyLogs = function (id) {
        return this.httpClient.get(environment.apiVersionPath + "/destroyprovisions/" + id + "/logs", { responseType: 'text' });
    };
    ProvisionsService.prototype.stopVms = function (id, userId) {
        return this.httpClient.post(environment.apiVersionPath + "/users/" + userId + "/provisions/" + id + "/deallocatevms", null);
    };
    ProvisionsService.prototype.startVms = function (id, userId) {
        return this.httpClient.post(environment.apiVersionPath + "/users/" + userId + "/provisions/" + id + "/startvms", null);
    };
    ProvisionsService.prototype.extend = function (id, userId) {
        return this.httpClient.post(environment.apiVersionPath + "/users/" + userId + "/provisions/" + id + "/extend", null);
    };
    ProvisionsService.prototype.setSelectedProv = function (provision) {
        if (provision) {
            this.selectedProv = provision;
        }
        else {
            this.selectedProv = null;
        }
    };
    ProvisionsService.prototype.getSelectedProv = function () {
        return this.selectedProv;
    };
    ProvisionsService.prototype.timeRunning = function (p) {
        var now = new Date();
        var runningFromTime = p.runningFrom ? new Date(p.runningFrom).getTime() : new Date(p.created).getTime();
        var totalRunningTime = p.timeRunning * 1000 * 60;
        if (p.statusVms !== 'Stopped' && p.statusVms !== 'Starting' && !p.isDestroyed) {
            totalRunningTime = totalRunningTime + Math.abs(now.getTime() - runningFromTime);
        }
        var authShutdownDate = new Date(runningFromTime);
        authShutdownDate.setDate(authShutdownDate.getDate() + this.RUNNING_PERIOD);
        var autoshutDown = authShutdownDate.getTime() - now.getTime();
        var durationAutoShutdown = moment.duration(autoshutDown);
        var duration = moment.duration(totalRunningTime);
        p.runningDays = Math.floor(duration.asDays());
        p.runningHours = duration.hours();
        p.runningMinutes = duration.minutes();
        p.autoshutdownDays = Math.floor(durationAutoShutdown.asDays());
        p.autoshutdownHours = durationAutoShutdown.hours();
        p.autoshutdownMinutes = durationAutoShutdown.minutes();
        if ((p.statusVms === 'Stopped' || p.statusVms === 'Starting') && !p.isDestroyed) {
            var autoDestroyDate = new Date(p.stoppedFrom);
            autoDestroyDate.setDate(autoDestroyDate.getDate() + this.STOP_PERIOD);
            var autoDestroy = autoDestroyDate.getTime() - now.getTime();
            var durationStop = moment.duration(autoDestroy);
            p.autoDestroyDays = Math.floor(durationStop.asDays());
            p.autoDestroyHours = durationStop.hours();
            p.autoDestroyMinutes = durationStop.minutes();
            var inactiveDate = new Date(p.stoppedFrom);
            var inactive = Math.abs(inactiveDate.getTime() - now.getTime());
            var durationInactive = moment.duration(inactive);
            p.inactiveDays = Math.floor(durationInactive.asDays());
            p.inactiveHours = durationInactive.hours();
            p.inactiveMinutes = durationInactive.minutes();
        }
    };
    ProvisionsService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient])
    ], ProvisionsService);
    return ProvisionsService;
}());
export { ProvisionsService };
//# sourceMappingURL=provisions.service.js.map