var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of, BehaviorSubject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { UsersService } from './users.service';
var AuthGuard = /** @class */ (function () {
    // Inject Router so we can hand off the user to the Login Page 
    function AuthGuard(_userService, httpClient, router) {
        this._userService = _userService;
        this.httpClient = httpClient;
        this.router = router;
        this.userInfo = new BehaviorSubject(null);
        var user = localStorage.getItem("user");
        this.userInfo.next(JSON.parse(user));
    }
    AuthGuard.prototype.canActivate = function (route) {
        var _this = this;
        console.log("CanActivate?");
        return this._userService.getMe().pipe(map(function (res) {
            localStorage.setItem("user", JSON.stringify(res));
            _this.userInfo.next(res);
            return true;
        }), catchError(function (err) {
            _this.clearUser();
            return of(false);
        }));
    };
    AuthGuard.prototype.clearUser = function () {
        localStorage.setItem("user", null);
        this.userInfo.next(null);
    };
    AuthGuard.prototype.getUserInfo = function () {
        return this.userInfo;
    };
    AuthGuard = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [UsersService, HttpClient, Router])
    ], AuthGuard);
    return AuthGuard;
}());
export { AuthGuard };
//# sourceMappingURL=auth.guard.js.map