var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable, EventEmitter } from '@angular/core';
var AlertService = /** @class */ (function () {
    function AlertService() {
        this.alertEmitter = new EventEmitter();
    }
    AlertService.prototype.showAlert = function (alert) {
        this.alertEmitter.emit(alert);
        if (this.to) {
            clearTimeout(this.to);
        }
        this.to = setTimeout(function () {
            this.alertEmitter.emit(null);
        }.bind(this), 5000);
    };
    AlertService.prototype.getAlertEmitter = function () {
        return this.alertEmitter;
    };
    AlertService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], AlertService);
    return AlertService;
}());
export { AlertService };
//# sourceMappingURL=alert.service.js.map