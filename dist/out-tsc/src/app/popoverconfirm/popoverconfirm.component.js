var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ElementRef, EventEmitter, ViewChild, Input, Output } from '@angular/core';
var PopoverconfirmComponent = /** @class */ (function () {
    function PopoverconfirmComponent(myElement) {
        this.onConfirm = new EventEmitter();
        this.element = myElement;
    }
    PopoverconfirmComponent.prototype.open = function () {
        var viewportOffset = this.element.nativeElement.getBoundingClientRect();
        var top = viewportOffset.top;
        var left = viewportOffset.left;
        this.popovercontent.nativeElement.style.display = 'block';
        this.popovercontent.nativeElement.style.position = 'absolute';
        if (this.dock.indexOf('left') !== -1) {
            this.left = -this.popovercontent.nativeElement.offsetWidth;
        }
        else if (this.dock.indexOf('right') !== -1) {
            this.left = this.element.nativeElement.offsetWidth;
        }
        this.top = 0;
        if (this.dock.indexOf('top') !== -1) {
            this.top = -this.popovercontent.nativeElement.offsetHeight;
        }
        else if (this.dock.indexOf('bottom') !== -1) {
            this.top = this.element.nativeElement.offsetHeight;
        }
        this.popovercontent.nativeElement.style.top = (top + this.top) + 'px';
        this.popovercontent.nativeElement.style.left = (left + this.left) + 'px';
    };
    PopoverconfirmComponent.prototype.ngOnInit = function () {
    };
    PopoverconfirmComponent.prototype.ok = function () {
        this.popovercontent.nativeElement.style.display = 'none';
        this.onConfirm.emit(true);
    };
    PopoverconfirmComponent.prototype.cancel = function () {
        this.popovercontent.nativeElement.style.display = 'none';
    };
    __decorate([
        ViewChild('popovercontent'),
        __metadata("design:type", ElementRef)
    ], PopoverconfirmComponent.prototype, "popovercontent", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], PopoverconfirmComponent.prototype, "dock", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], PopoverconfirmComponent.prototype, "buttonConfig", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], PopoverconfirmComponent.prototype, "popupConfig", void 0);
    __decorate([
        Output(),
        __metadata("design:type", Object)
    ], PopoverconfirmComponent.prototype, "onConfirm", void 0);
    PopoverconfirmComponent = __decorate([
        Component({
            selector: 'app-popoverconfirm',
            templateUrl: './popoverconfirm.component.html',
            styleUrls: ['./popoverconfirm.component.scss']
        }),
        __metadata("design:paramtypes", [ElementRef])
    ], PopoverconfirmComponent);
    return PopoverconfirmComponent;
}());
export { PopoverconfirmComponent };
//# sourceMappingURL=popoverconfirm.component.js.map