import { async, TestBed } from '@angular/core/testing';
import { PopoverconfirmComponent } from './popoverconfirm.component';
describe('PopoverconfirmComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [PopoverconfirmComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(PopoverconfirmComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=popoverconfirm.component.spec.js.map