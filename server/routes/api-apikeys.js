const express = require('express');
const router = express.Router();
const db = require('qmi-cloud-common/mongo');
const passport = require('../passport');


/**
 * @swagger
 * /apikeys:
 *    get:
 *      description: Get all API keys
 *      summary: Get all API keys
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: API KEYS
 */
router.get('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      const result = await db.apiKey.get();
      return res.json(result);
    } catch (error) {
      next(error);
    }
});

/**
 * @swagger
 * /apikeys/{userId}:
 *    post:
 *      description: Create an API for the userId
 *      summary: Create an API for the userId
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: userId
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: API KEY
 */
router.post('/:userId', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      const  user = await db.user.getById(req.params.userId);
      if ( !user ) {
          res.status(404).json({"err": "user not found"});
      }
      var body = {
          user: req.params.userId
      }
      const result = await db.apiKey.add(body);
      return res.json(result);
    } catch (error) {
      next(error);
    }
});

/**
 * @swagger
 * /apikeys/{id}:
 *    put:
 *      description: Deactivate API Key
 *      summary: Deactivate API Key
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      responses:
 *        200:
 *          description: API KEY
 */
router.put('/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
        const result = await db.apiKey.update(req.params.id, {"isActive": false});
        return res.json(result);
    } catch (error) {
      next(error);
    }
});

module.exports = router;