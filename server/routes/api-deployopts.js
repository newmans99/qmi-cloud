const express = require('express')
const router = express.Router()
const db = require('qmi-cloud-common/mongo');
const passport = require('../passport');


/**
 * @swagger
 * /deployopts:
 *    get:
 *      description: Get all deployopts
 *      summary: Get all deployopts
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Subscriptions
 */
router.get('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      const result = await db.subscription.get();
      return res.json(result);
      
    } catch (error) {
      next(error);
    }
  });
  
/**
 * @swagger
 * /deployopts:
 *    post:
 *      description: Add new deployopts
 *      summary: Add new deployopts
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                subsId:
 *                  type: string
 *                description:
 *                  type: string
 *      responses:
 *        200:
 *          description: deployopts
 */
router.post('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
        console.log("BODY", req.body);
        const result = await db.subscription.add(req.body);
        return res.json(result);
    } catch (error) {
        next(error);
    }
});


  /**
 * @swagger
 * /deployopts/{id}:
 *    put:
 *      description: Update deployopts
 *      summary: Update deployopts
 *      tags:
 *        - admin
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *        - in: body
 *          name: body
 *          description: deployopts object
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: deployopts
 */
router.put('/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.subscription.update(req.params.id, req.body);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

 /**
 * @swagger
 * /deployopts/{id}:
 *    delete:
 *      description: Delete deployopts
 *      summary: Delete deployopts
 *      tags:
 *        - admin
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 */
router.delete('/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.subscription.del(req.params.id);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});


module.exports = router;