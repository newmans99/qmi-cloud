const express = require('express');
const router = express.Router();
const db = require('qmi-cloud-common/mongo');
const passport = require('../passport');


/**
 * @swagger
 * /notifications:
 *    get:
 *      description: Get all notifications
 *      summary: Get all notifications
 *      tags:
 *        - admin
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Notifications
 */
router.get('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      const result = await db.notification.get();
      return res.json(result);
    } catch (error) {
      next(error);
    }
  });

module.exports = router;