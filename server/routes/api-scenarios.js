const express = require('express')
const router = express.Router()
const db = require('qmi-cloud-common/mongo');
const passport = require('../passport');


router.get('/all', passport.ensureAuthenticated, async (req, res, next) => {
  try {
    const result = await db.scenario.get();
    return res.json(result);
      
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /scenarios:
 *    get:
 *      description: Get scenarios
 *      summary: Get scenarios
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Scenarios
 */
router.get('/', passport.ensureAuthenticated, async (req, res, next) => {
    try {
      const filter = {};
      if (req.user.role === "user") {
        filter.isAdminOnly = false;
      }
      filter.isDisabled = filter.isDisabled || false;
      const result = await db.scenario.get(filter);
      return res.json(result);
      
    } catch (error) {
      next(error);
    }
  });

/**
 * @swagger
 * /scenarios/vmtypes:
 *    get:
 *      description: Get scenarios VM Types
 *      summary: Get scenarios VM Types
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: VM Types
 */
router.get('/vmtypes', passport.ensureAuthenticated, async (req, res, next) => {
  try {
    const result = await db.vmtype.get();

    return res.json(result);
    
  } catch (error) {
    next(error);
  }
});


/**
 * @swagger
 * /scenarios/vmtypes:
 *    post:
 *      description: Create new VM Type
 *      summary: Create new VM Type
 *      tags:
 *        - admin
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                type:
 *                  type: string
 *                desc:
 *                  type: string
 *                costHour:
 *                  type: number
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: VM Type
 */
router.post('/vmtypes', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.vmtype.add(req.body);

    return res.json(result);
    
  } catch (error) {
    next(error);
  }
});


/**
 * @swagger
 * /scenarios/vmtypes/{id}:
 *    put:
 *      description: Update VM type
 *      summary: Update VM type
 *      tags:
 *        - admin
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *        - in: body
 *          name: body
 *          description: VM Type object
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: VM Type
 */
router.put('/vmtypes/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.vmtype.update(req.params.id, req.body);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * scenarios/vmtypes/{id}:
 *    delete:
 *      description: Delete VM Type
 *      summary: Delete VM Type
 *      tags:
 *        - admin
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 */
router.delete('/vmtypes/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.vmtype.del(req.params.id);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});
  
  /**
   * @swagger
   * /scenarios:
   *    post:
   *      description: Add new scenario
   *      summary: Add new scenario
   *      tags:
   *        - admin
   *      produces:
   *        - application/json
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              type: object
   *              properties:
   *                name:
   *                  type: string
   *                title:
   *                  type: string
   *                description:
   *                  type: string
   *                version:
   *                  type: string
   *                isAdminOnly:
   *                  type: boolean
   *                isExternal:
   *                  type: boolean
   *                availableProductVersions:
   *                  type: array
   *                  items: 
   *                    type: object
   *                    properties:
   *                      product:
   *                        type: string
   *                      vmTypeDefault:
   *                        type: string
   *                      index:
   *                        type: string
   *                      versions:
   *                        type: array
   *                        items:
   *                          type: object
   *                          properties:
   *                            name:
   *                              type: string
   *                            image:
   *                              type: string
   *      responses:
   *        200:
   *          description: Scenario
   */
  router.post('/', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
    try {
      const result = await db.scenario.add(req.body);
      return res.json(result);
    } catch (error) {
      next(error);
    }
  });


/**
 * @swagger
 * /scenarios/{id}:
 *    put:
 *      description: Update scenario
 *      summary: Update scenario
 *      tags:
 *        - admin
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *        - in: body
 *          name: body
 *          description: Scenario object
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: Scenario
 */
router.put('/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.scenario.update(req.params.id, req.body);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});

/**
 * @swagger
 * /scenarios/{id}:
 *    delete:
 *      description: Delete scenario
 *      summary: Delete scenario
 *      tags:
 *        - admin
 *      parameters:
 *        - name: id
 *          in: path
 *          type: string
 *          required: true
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 */
router.delete('/:id', passport.ensureAuthenticatedAndAdmin, async (req, res, next) => {
  try {
    const result = await db.scenario.del(req.params.id);
    return res.json(result);
  } catch (error) {
    next(error);
  }
});


module.exports = router;