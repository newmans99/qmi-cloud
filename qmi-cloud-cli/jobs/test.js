const db = require('qmi-cloud-common/mongo');

/*var scenarios = ['azqmi-qs-sn', 'azqmi-qdc-qs', 'azqmi-qdc-ss', 'azqmi-qs-qib'];
db.models.Provision.updateMany({"scenario": { $in: scenarios}}, {"isExternalAccess": true}, function(err, res){
    if (err) { 
        console.log("error", err);
        process.exit(1);
    } else { 
        console.log("Good", res);
        process.exit(0);
    }
});*/

/*var promise = db.models.Subscription.create({
    subsId: "62ebff8f-c40b-41be-9239-252d6c0c8ad9"
});
promise.then(function (subs) {

        console.log("Good", subs);
        process.exit(0);
    
}).catch(function(err){
    console.log("error", err);
        process.exit(1);
})*/

async function getScenario(){
    var res = await db.scenario.get();
    console.log("res", res.results);
}


/*db.models.Scenario.updateMany({"name": { $exists: true}}, {"subscription": "5ec23bd0999e8a00193725b8"}, function(err, res){
    if (err) { 
        console.log("error", err);
        process.exit(1);
    } else { 
        console.log("Good", res);
        process.exit(0);
    }
});*/

//getScenario();

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

async function getProvisions(filter){
    return db.provision.get(filter);
}


/*getProvisions().then(async function(res){
    console.log(res.results.length);
    await asyncForEach(res.results, async function(prov){
        if ( prov.vmType ) {
            //console.log("vmType", prov.vmType, prov.vmImage)
            prov["vmImage"] = {
                "vm1": {
                    "vmType": prov.vmType,
                    "diskSizeGb": 128
                }
            }
            prov.vmType = null;
            await prov.save();
        }
    });
    process.exit(0);
});*/

/*db.models.Provision.updateMany({"nodeCount": { $exists: true}}, { $unset: { nodeCount: "" } }, function(err, res){
    if (err) { 
        console.log("error", err);
        process.exit(1);
    } else { 
        console.log("Good", res);
        process.exit(0);
    }
});*/

/*db.models.Scenario.updateMany({"nodeCount": { $exists: true}}, { $unset: { nodeCount: "" } }, function(err, res){
    if (err) { 
        console.log("error", err);
        process.exit(1);
    } else { 
        console.log("Good", res);
        process.exit(0);
    }
});*/

/*db.models.Provision.updateMany({}, { "scenarioVersion": "1.0" }, function(err, res){
    if (err) { 
        console.log("error", err);
        process.exit(1);
    } else { 
        console.log("Good", res);
        process.exit(0);
    }
});*/

/*getProvisions({
        "isDestroyed": true,
        "statusVms": "Stopped"
}).then(async function(res){
    console.log(res.results.length);
    await asyncForEach(res.results, async function(prov){
        //if ( prov.destroy ) {
            var runningFrom = new Date(prov.runningFrom).getTime();
            var updated;
            if ( prov.destroy ) {
                updated = new Date(prov.destroy.updated).getTime();
            } else {
                updated = new Date(prov.updated).getTime();
            }
            var mins = Math.floor( (updated-runningFrom)/1000/60);
            if ( !isNaN(mins) ) {
                var newTimeRunning = (prov.timeRunning-mins);

                if ( newTimeRunning > 0 ) {
                    console.log(prov._id, "timeRunning="+prov.timeRunning, "    offset="+mins, " timeRunningNew="+newTimeRunning, prov.destroy? "OK" : "BUUUH");

                    prov.timeRunning = newTimeRunning;
                    await prov.save();
                    console.log("saved!");
                }
            }
        //}
    });
    process.exit(0);
})*/


/*getProvisions({
        "isDestroyed": true
}).then(async function(res){
    console.log(res.results.length);
    var count = 0;
    await asyncForEach(res.results, async function(prov){
        //if ( prov.destroy ) {
            var actualDestroyDate;
            if ( prov.destroy ) {
                actualDestroyDate = prov.destroy.updated;
                prov.actualDestroyDate = actualDestroyDate;
                await prov.save();
                count = count + 1;
                console.log("saved!", count);
            }
                     
        //}
    });
    process.exit(0);
})*/

getProvisions({
        "isDestroyed": true, "actualDestroyDate": {"$exists":false}, "isDeleted": true
}).then(async function(res){
    console.log(res.total, res.count);
    var count = 0;
    await asyncForEach(res.results, async function(prov){
        prov.actualDestroyDate = prov.updated;
        await prov.save();
        count = count + 1;
        console.log("saved!", count);                     
    });
    process.exit(0);
})



