const db = require('qmi-cloud-common/mongo');
var initDataSubscriptions = require("../initdata/subscriptions.json");
var scenarios1 = require("../initdata/scenariosQMI-Automation.json");
var scenarios2 = require("../initdata/scenariosQlik-Secure.json");
var vmTypes = require("../initdata/vmtypes.json");


async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

async function init() { 
    // Adding VmTypes
    await asyncForEach(vmTypes, async function(v){
        await db.vmtype.add(v);
    });

    // Adding Subscriptions
    await asyncForEach(initDataSubscriptions, async function(s){
        await db.subscription.add(s);
    });
    
    var subscription;
    //QMI Automation
    subscription = await db.subscription.getOne({"description": "QMI Automation"});
    await asyncForEach(scenarios1, async function(s){
        s.subscription = subscription._id;
        await db.scenario.add(s);
    });

    //Qlik-Secure
    subscription = await db.subscription.getOne({"description": "Qlik-Secure"});
    await asyncForEach(scenarios2, async function(s){
        s.subscription = subscription._id;
        await db.scenario.add(s);
    });

    console.log("Done!");
    process.exit(0);

}

init().catch(function(err){
    console.log(err);
    process.exit(1);
});

module.exports.init = init;
