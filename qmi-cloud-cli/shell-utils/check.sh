
BASEDIR=$(dirname "$0")

helpFunction()
{
   echo ""
   echo "Usage: $0 -s script"
   echo -e "\t-s Type of script ( checkstop | checkdestroy | initdb )"
   exit 1 # Exit script after printing help
}

helpFunctionCheck()
{
   echo ""
   echo "Usage: $0 -s script -t type -p period -r realortest"
   echo -e "\t-s Type of script ( checkstop | checkdestroy | initdb )"
   echo -e "\t-t Type of script (warning | exec)"
   echo -e "\t-p Period (number of days)"
   echo -e "\t-r Type of script (test | real)"
   exit 1 # Exit script after printing help
}

while getopts "s:t:p:r:" opt
do
   case "$opt" in
      s ) script="$OPTARG" ;;
      t ) type="$OPTARG" ;;
      p ) period="$OPTARG" ;;
      r ) realortest="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

if [ -z "$script" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

if [ "$script" = "checkstop" ] || [ "$script" = "checkdestroy" ]
then
   if [ -z "$type" ] || [ -z "$period" ] || [ -z "$realortest" ]
   then
      echo "Some or all of the parameters are empty";
      helpFunctionCheck
   else
      if [ "$script" = "checkstop" ] 
      then
         $BASEDIR/checkstop.sh $type $period $realortest
      fi

      if [ "$script" = "checkdestroy" ] 
      then
         $BASEDIR/checkdestroy.sh $type $period $realortest
      fi
   fi
elif [ "$script" = "initdb" ] 
then
   if [ -z "$MONGO_URI" ]
   then
         echo "\$MONGO_URI is empty"
         exit
   else
         echo "\$MONGO_URI=$MONGO_URI"
         node $BASEDIR/../jobs/db-init.js
   fi 
fi

