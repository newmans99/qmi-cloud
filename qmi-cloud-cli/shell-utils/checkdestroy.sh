BASEDIR=$(dirname "$0")
d=`date`
echo "------ $d"
echo "------ SCRIPT: $0"
echo "------ TYPE: $1"
echo "------ PERIOD: $2"
echo "------ TEST/REAL: $3"

if [ -z "$MONGO_URI" ]
then
      echo "\$MONGO_URI is empty"
      exit
else
      echo "\$MONGO_URI=$MONGO_URI"
fi

if [ -z "$API_KEY" ]
then
      echo "\$API_KEY is empty"
      exit
else
      echo "\$API_KEY=$API_KEY"
fi

node $BASEDIR/../jobs/destroy5.js $1 $2 $3
