#!/bin/bash

id=`docker ps | grep 'mongo:4.2' | awk '{ print $1 }'`
d=$(date +%Y-%m-%d-T%H:%M:%S%z)

docker exec $id sh -c 'exec mongodump --uri="mongodb://root:example@mongo/qmicloud?authSource=admin" --archive' > $1/qmicloud-$d.archive