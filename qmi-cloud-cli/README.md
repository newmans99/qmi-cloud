# Examples

```
docker run --net=host \
    -e MONGO_URI="mongodb://root:example@localhost:27017/qmicloud?authSource=admin" \
    -e API_KEY="c229219ccdd72d11e8ea253fd3876d247e5f489c9c84922cabdfb0cc194d8ff398a8d8d6528d8241efc99add2207e0ec75122a1b2c5598cc340cbe6b7c3c0dbf"  \
    qlikgear/qmi-cloud-cli:latest -s checkdestroy -t warning -p 4 -r test
```

```
docker run --net=host \
    -e MONGO_URI="mongodb://root:example@localhost:27017/qmicloud?authSource=admin" \
    qlikgear/qmi-cloud-cli:latest -s initdb
```

```
docker run --net=host -e MONGO_URI=$MONGO_URI -e API_KEY=$API_KEY qlikgear/qmi-cloud-cli:latest -s checkdestroy -t warning -p 20 -r test
```

```
docker run --net=host -e MONGO_URI=$MONGO_URI -e API_KEY=$API_KEY qlikgear/qmi-cloud-cli:latest -s checkstop -t warning -p 4 -r test
```