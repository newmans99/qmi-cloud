const Docker = require('dockerode');
const docker = new Docker({
    'socketPath':  '/home/docker.sock'
    //'socketPath':  '/var/run/docker.sock'
});
const fs = require("fs");
const DOCKERIMAGE = process.env.DOCKERIMAGE_AZURE_POWERSHELL || "mcr.microsoft.com/azure-powershell";
//const cmd = `docker run --net=host -w /myapp -v ${FOLDER}:/myapp mcr.microsoft.com/azure-powershell pwsh appgw.ps1 -ApplicationGatewayName ${appGwName}`;

const appgateway = function( provision, scenario ) {
    
    if ( provision.status === 'provisioned' && scenario.isWafPolicyAppGw && provision.isExternalAccess ) {
        var provision_id = provision._id.toString();
        var processStream = fs.createWriteStream(provision.logFile, {flags:'a'});
        var name = 'qmi-azureps-appgw-'+provision_id;
        console.log(`AzurePS: will spin up container: ${name}`);

        return docker.run(DOCKERIMAGE, ['pwsh', 'appgw.ps1', "-ProvisionId", provision_id ], processStream, { 
            "name": name,
            "WorkingDir": "/myapp",
            "HostConfig": {
                "Binds": [
                    `${provision.path}/shell:/myapp`
                ],
                "NetworkMode": "host"
            }
        }).then(function(data) {
            var output = data[0];
            var container = data[1];
            console.log(`AzurePS: ${name} (${container.id}) has finished with code: ${output.StatusCode}`);
            return container.remove();
        }).then(function() {
            console.log(`AzurePS: ${name} removed!`);
            return Promise.resolve(provision);
        });

    } else {
        return Promise.resolve(provision);
    }
   
};

const createimage = function( provision, scenario ) {
    
    if ( provision.status === 'provisioned' && scenario.newImageName ) {
        var provision_id = provision._id.toString();
        var processStream = fs.createWriteStream(provision.logFile, {flags:'a'});
        var name = 'qmi-azureps-createimage-'+provision_id;

        let rgName = provision.scenario.toUpperCase();
        rgName = rgName + "-" + provision_id;

        let imageName = scenario.newImageName + "-" + new Date().getTime();

        console.log(`AzurePS: will spin up container: ${name}`);

        return docker.run(DOCKERIMAGE, ['pwsh', 'createimage.ps1', "-rgName", rgName, "-imageName", imageName ], processStream, { 
            "name": name,
            "WorkingDir": "/myapp",
            "HostConfig": {
                "Binds": [
                    `${provision.path}/shell:/myapp`
                ],
                "NetworkMode": "host"
            }
        }).then(function(data) {
            var output = data[0];
            var container = data[1];
            console.log(`AzurePS: ${name} (${container.id}) has finished with code: ${output.StatusCode}`);
            return container.remove();
        }).then(function() {
            console.log(`AzurePS: ${name} removed!`);
            return Promise.resolve(provision);
        });

    } else {
        return Promise.resolve(provision);
    }
   
};

module.exports.appgateway = appgateway;
module.exports.createimage = createimage;