# QMI Cloud

## Pre-requisites
- Docker
- Docker-Compose
- A file named secrets.json with this JSON information (as Manuel Romero for this details)

```json
{
    "AZURE_TENANT_ID" : "xxxxxxxx",
    "AZURE_CLIENT_ID": "yyyyyyyy",
    "AZURE_CLIENT_SECRET": "zzzzzzzz"
}
```


## Run it

It will build necessary images if they don't exist.

```shell
    git clone git@gitlab.com:qmi/qmi-cloud.git
    cd qmi-cloud
    docker-compose up
```

Wait until this line shows up:

```qmi-cloud-app | MongoDB connected...```


On another terminal, run: 

```shell
docker container ls
```

Should show these containers:
```
CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS              PORTS                    NAMES
37db1a78ad8a        mongo-express               "tini -- /docker-ent…"   32 minutes ago      Up About a minute   0.0.0.0:8081->8081/tcp   qmi-cloud-mongo-express
eb82e43c47ad        qlikgear/qmi-cloud-worker   "docker-entrypoint.s…"   4 hours ago         Up 2 minutes                                 qmi-cloud-worker
ad01a01a4903        qlikgear/qmi-cloud-app      "docker-entrypoint.s…"   4 hours ago         Up 2 minutes        0.0.0.0:3000->3000/tcp   qmi-cloud-app
8bb7856f90b6        mongo                       "docker-entrypoint.s…"   4 hours ago         Up 2 minutes        27017/tcp                qmi-cloud-mongo
94a1414554aa        redis                       "redis-server --appe…"   4 hours ago         Up 2 minutes        6379/tcp                 qmi-cloud-redis
```

## TEST 

Go to: http://localhost:3000

### Authentication
Authentication with Azure AD (Qlik).

### Bull QUEUE system
It is used to queue Terraform executions in Docker containers.

UI: http://localhost:3000/arena


### Mongo / Mongo Express Client
Mongo is for managment.

UI: http://localhost:8081

Basic-Auth: qlik / Qlik1234

Database: qmicloud

### API / Swagger API-DOCS
It shows available endpoints

http://localhost:3000/api-docs




