# Stage 1: 
FROM node:13.8-alpine AS sources

RUN apk --no-cache add yarn
  
WORKDIR /var/www/app

ADD ./package.json ./
ADD ./yarn.lock ./
ADD ./qmi-cloud-common ./qmi-cloud-common

RUN yarn install --production

# Stage 2: 
FROM node:13.8-alpine AS production
WORKDIR /var/www/app
COPY --from=sources /var/www/app/node_modules ./node_modules
COPY --from=sources /var/www/app/package.json ./package.json
COPY ./server ./server
COPY ./dist ./dist

EXPOSE 3000
EXPOSE 3100

CMD ["node", "-r", "esm", "server/server"]
