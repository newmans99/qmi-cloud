import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq-component',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onLoad(event): void {
    console.log("nice", event);
  }

  onError(event): void {
    console.log("error", event);
  }

}
