import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthGuard } from '../services/auth.guard';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: []
})
export class HomeComponent implements OnInit {

  user;
  subs: Subscription;

  constructor( private _auth: AuthGuard){
    this.subs = this._auth.getUserInfo().subscribe( value => {
      this.user = value;
      console.log("USER", this.user);
    });
  }
  
  ngOnInit() {
    
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  popupConfirm(): void {
    console.log("Confirmed");
  }
  
}
