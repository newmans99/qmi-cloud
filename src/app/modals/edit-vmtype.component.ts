import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';
import { ScenariosService } from '../services/scenarios.service';

@Component({
  selector: 'qmi-new-vmtype',
  templateUrl: './edit-vmtype.component.html',
  styleUrls: []
})
export class VmTypeModalComponent implements OnInit, OnDestroy {
  
  item;
  action: Subject<any> = new Subject();
  sendData : any = {};
 
  constructor( public modalRef: MDBModalRef, private _scenariosService: ScenariosService ) {}

  ngOnInit() {
    if (this.item) {
      this.sendData = JSON.parse(JSON.stringify(this.item))
    }     
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {
      
      console.log("sendData", this.sendData);

      if (this.sendData._id){
        this._scenariosService.updateScenarioVmtype(this.sendData._id, this.sendData).subscribe( res=> {
          console.log("done", res);
          this.action.next("DONE!!!");
          this.modalRef.hide();
        });
      } else {
        this._scenariosService.createScenarioVmtype(this.sendData).subscribe( res=> {
          console.log("done", res);
          this.action.next("DONE!!!");
          this.modalRef.hide();
        });
      }
      
      
  }

  delete() : void {    
    this._scenariosService.deleteScenarioVmtype(this.sendData._id).subscribe( res=> {
      console.log("done", res);
      this.action.next("DONE!!!");
      this.modalRef.hide();
    });
  }

  checkOnchange($event, field) {
    this.sendData[field] = $event.checked;
  }

}