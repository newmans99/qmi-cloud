import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { AlertService } from '../services/alert.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'qmi-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {
  
  @ViewChild('qmialert', { static: true }) alertEl: ElementRef;
  subscription: Subscription;
  alert : any = null;

  constructor(private _alertService: AlertService) {}

  ngOnInit() {
    this.subscription = this._alertService.getAlertEmitter().subscribe(function(data){
        this.alert = data;
    }.bind(this));     
  }

  ngOnDestroy() { 
    this.subscription.unsubscribe();  
  }

  closeAlert() {
      this.alert = null;
  }

}
