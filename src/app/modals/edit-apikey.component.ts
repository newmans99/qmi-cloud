import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { ScenariosService } from '../services/scenarios.service';
import { SubscriptionsService } from '../services/deployopts.service';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'qmi-new-apikey',
  templateUrl: './edit-apikey.component.html',
  styleUrls: ['./edit-apikey.component.scss']
})
export class ApikeyModalComponent implements OnInit, OnDestroy {
  
  apiKey;
  action: Subject<any> = new Subject();
  users;
  selectedUser;

  sendData : any = {};
 
  constructor( public modalRef: MDBModalRef, private _usersService: UsersService ) {}

  ngOnInit() {
    this._usersService.getUsers().subscribe(res=> {
      this.users = res.results;
        console.log("apiKey",this.apiKey);
        if (this.apiKey) {
          this.sendData = JSON.parse(JSON.stringify(this.apiKey))
        }
        if (this.apiKey.user ) {
          this.selectedUser = this.apiKey.user._id;
        }
    })
      
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {
      
      this.sendData.user = this.selectedUser;
      console.log("sendData", this.sendData);

      this._usersService.addApikey(this.sendData.user).subscribe( res=> {
        console.log("done", res);
        this.action.next("DONE!!!");
        this.modalRef.hide();
      });
      
  }

  delete() : void {    
    this._usersService.delApikey(this.sendData._id).subscribe( res=> {
      console.log("done", res);
      this.action.next("DONE!!!");
      this.modalRef.hide();
    });   
  }

}