import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { ScenariosService } from '../services/scenarios.service';

@Component({
  selector: 'qmi-new-provision',
  templateUrl: './new-provision.component.html',
  styleUrls: ['./new-provision.component.scss']
})
export class NewProvisionConfirmComponent implements OnInit, OnDestroy {
  
  scenario;
  action: Subject<any> = new Subject();
  shortDesc: string;
  sendData = {
    description: "",
    servers: null,
    isExternalAccess: false,
  };
  selectedProductVersion: any = {};
  selectedVmType: any = {};
  selectedNodeCount: any = {};
  selectedDiskSizeGb: any = {};
  vmTypesSub: Subscription;
  vmTypes: any;
  servers: any = {};
  constructor( public modalRef: MDBModalRef, private _scenariosService: ScenariosService ) {}

  ngOnInit() {
    this.vmTypesSub = this._scenariosService.getScenarioVmtypes().subscribe ( res => {
      this.vmTypes = res.results.filter(v=>!v.disabled);

      if ( this.scenario.availableProductVersions.length ) {
        this.scenario.availableProductVersions.forEach(server => {
            if (server.vmTypeDefault) {
              this.selectedVmType[server.index] = server.vmTypeDefault;
            }
            if ( server.nodeCount ) {
              this.selectedNodeCount[server.index] = server.nodeCount;
            }

            this.selectedDiskSizeGb[server.index] = server.diskSizeGbDefault || 500;

            if ( server.versions && server.versions.length ) {
              let lastIndex = server.versions.length - 1;
              this.selectedProductVersion[server.index] = server.productVersionDefault? server.productVersionDefault : server.versions[lastIndex].name;
            }        
        });

        
      }
      
      this.vmTypesSub.unsubscribe();
    })
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {
      if (!this.sendData.description || this.sendData.description.trim() === "") {
          return;
      }
      this.sendData.servers = {};
      for (let key in this.selectedVmType){
        if (!this.sendData.servers[key]) {
          this.sendData.servers[key] = {};
        }
        if (this.selectedVmType[key]) {
          this.sendData.servers[key].vmType = this.selectedVmType[key];
        }
        if ( this.selectedNodeCount[key] ) {
          this.sendData.servers[key].nodeCount = this.selectedNodeCount[key];
        }

        if ( this.selectedDiskSizeGb[key] ) {
          this.sendData.servers[key].diskSizeGb = this.selectedDiskSizeGb[key];
        }

        this.scenario.availableProductVersions.forEach(server => {
          server.versions.forEach(v=> {
            if (v.name === this.selectedProductVersion[key]){
              this.sendData.servers[key].version = v;
            }
          })
        });
      }
      console.log("sendData", this.sendData);
      this.action.next(this.sendData);
      this.modalRef.hide();
  }

  checkOnchange($event) {
    console.log("Checked?", $event.checked);
    this.sendData.isExternalAccess = $event.checked;
  }

}