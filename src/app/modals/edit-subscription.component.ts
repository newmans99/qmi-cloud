import { Component, OnInit, OnDestroy } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject, Subscription } from 'rxjs';
import { SubscriptionsService } from '../services/deployopts.service';

@Component({
  selector: 'qmi-new-subscription',
  templateUrl: './edit-subscription.component.html',
  styleUrls: ['./edit-subscription.component.scss']
})
export class SubscriptionModalComponent implements OnInit, OnDestroy {
  
  subscription;
  action: Subject<any> = new Subject();
  subscriptions;

  sendData : any = {
  };
 
  constructor( public modalRef: MDBModalRef, private _subscriptionsService: SubscriptionsService ) {}

  ngOnInit() {
      if (this.subscription) {
        this.sendData = JSON.parse(JSON.stringify(this.subscription))
      }
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {
      
      console.log("sendData", this.sendData);
      if ( !this.sendData.vnetExists ) {
        this.sendData.subnetId = null;
        this.sendData.appGwSubnetId  = null;
      } else {
        if (!this.sendData.subnetId || !this.sendData.appGwSubnetId) {
          return;
        }
      }
      //this.action.next(this.sendData);
      if ( this.sendData._id) {
        var id = this.sendData._id.toString();
        this.sendData._id = undefined;
        this._subscriptionsService.updateSubscription(id, this.sendData).subscribe( res=> {
          console.log("done", res);
          this.action.next("DONE!!!");
          this.modalRef.hide();
        });
      } else {
        this._subscriptionsService.addSubscription(this.sendData).subscribe( res=> {
          console.log("done", res);
          this.action.next("DONE!!!");
          this.modalRef.hide();
        });
      }
      
      
  }

  checkOnchange($event, field) {
    console.log("Checked?", $event.checked);
    this.sendData[field] = $event.checked;
  }

  delete() : void {    
    this._subscriptionsService.deleteScenario(this.sendData._id).subscribe( res=> {
      console.log("done", res);
      this.action.next("DONE!!!");
      this.modalRef.hide();
    });   
  }

}