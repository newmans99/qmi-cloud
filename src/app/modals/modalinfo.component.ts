import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';

@Component({
  selector: 'qmi-modalinfo',
  templateUrl: './modalinfo.component.html',
  styleUrls: ['./modalinfo.component.scss']
})
export class ModalInfoComponent implements OnInit, OnDestroy {
  
  info;

  constructor( public modalRef: MDBModalRef ) {}

  ngOnInit() {
     
  }

  ngOnDestroy() { 
    
  }

}
