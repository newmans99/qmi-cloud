import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { MDBModalRef } from 'angular-bootstrap-md';
import { Subject } from 'rxjs';

@Component({
  selector: 'qmi-modalconfirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ModalConfirmComponent implements OnInit, OnDestroy {
  
  info;
  action: Subject<any> = new Subject();

  constructor( public modalRef: MDBModalRef ) {}

  ngOnInit() {
    if (!this.info.buttonColor) {
      this.info.buttonColor = "danger";
    }
  }

  ngOnDestroy() { 
    
  }

  confirm() : void {
      this.action.next();
      this.modalRef.hide();
  }

}
