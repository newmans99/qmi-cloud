import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  
  alertEmitter = new EventEmitter();

  constructor() { }
  to;

  showAlert(alert) : void {
    this.alertEmitter.emit(alert);
    if ( this.to ) {
      clearTimeout(this.to);
    }
    this.to = setTimeout (function(){
        this.alertEmitter.emit(null);
     }.bind(this), 5000);
  }

  getAlertEmitter(): EventEmitter<any> {
    return this.alertEmitter;
  }

}
