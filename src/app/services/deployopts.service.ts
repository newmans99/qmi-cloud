import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsService {

  constructor( private httpClient: HttpClient ) { }

  getSubscriptions() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/deployopts`);
  }

  addSubscription(data) : Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/deployopts`, data);
  }

  updateSubscription(id, patchData) : Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/deployopts/${id}`, patchData);
  }

  deleteScenario(id) : Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/deployopts/${id}`);
  }

}
