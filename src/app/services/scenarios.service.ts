import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ScenariosService {

  constructor( private httpClient: HttpClient ) { }

  getScenarios() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/scenarios`);
  }

  getScenariosAll() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/scenarios/all`);
  }

  updateScenario(id, patchData) : Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/scenarios/${id}`, patchData);
  }

  addScenario(data) : Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/scenarios`, data);
  }

  deleteScenario(id) : Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/scenarios/${id}`);
  }


  getScenarioVmtypes() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/scenarios/vmtypes`);
  }

  createScenarioVmtype(data) : Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/scenarios/vmtypes`, data);
  }

  updateScenarioVmtype(id, data) : Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/scenarios/vmtypes/${id}`, data);
  }

  deleteScenarioVmtype(id) : Observable<any> {
    return this.httpClient.delete(`${environment.apiVersionPath}/scenarios/vmtypes/${id}`);
  }

}
