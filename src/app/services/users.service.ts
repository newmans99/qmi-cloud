import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor( private httpClient: HttpClient ) { }

  getMe() {
    return this.httpClient.get(`${environment.apiVersionPath}/users/me`);
  }
  
  getUsers() : Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/users`);
  }

  updateUser(userId, patchData): Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/users/${userId}`, patchData);
  }

  getNotifications(): Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/notifications`);
  }

  getApiKeys(): Observable<any> {
    return this.httpClient.get(`${environment.apiVersionPath}/apikeys`);
  }

  addApikey(userId): Observable<any> {
    return this.httpClient.post(`${environment.apiVersionPath}/apikeys/${userId}`, null);
  }

  delApikey(id): Observable<any> {
    return this.httpClient.put(`${environment.apiVersionPath}/apikeys/${id}`, null);
  }
}
