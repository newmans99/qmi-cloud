import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProvisionsComponent }   from './provisions/provisions.component';
import { AdminComponent }   from './admin/admin.component';
import { HomeComponent }   from './home/home.component';
import { AuthGuard } from './services/auth.guard';
import { FaqComponent } from './faq/faq.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'faq', component: FaqComponent},
  { path: 'provisions', component: ProvisionsComponent, canActivate: [AuthGuard]},
  { path: 'admin', component: AdminComponent, canActivate: [AuthGuard]},
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
