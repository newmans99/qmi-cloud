import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  
  sections = ['Provisions', 'Scenarios',  'Scenario Deploy Opts', 'Users', 'Notifications','API keys', 'VM Types'];
  tab : string =  'Provisions';

  constructor() { }

  ngOnInit() {
  }

  tabSelect($event, tab) {
    $event.preventDefault();
    $event.stopPropagation();
    this.tab = tab;
  }

}
