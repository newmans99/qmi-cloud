import { Component, OnInit } from '@angular/core';
import { AuthGuard } from '../../services/auth.guard';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  user;
  subs: Subscription;

  constructor( private _auth: AuthGuard){
    this.subs = this._auth.getUserInfo().subscribe( value => {
      this.user = value;
      console.log("USER", this.user);
    });
  }

  ngOnInit() {
  
  }

  logout($event): void {
    $event.preventDefault();
    $event.stopPropagation();
    window.location.href = "/logout";
    this._auth.clearUser();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

}
