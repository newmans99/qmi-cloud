import { MdbTablePaginationComponent, MdbTableDirective, MDBModalService } from 'angular-bootstrap-md';

import { Component, OnInit, ViewChild, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { ScenariosService } from '../services/scenarios.service';
import { SubscriptionModalComponent } from '../modals/edit-subscription.component';
import { SubscriptionsService } from '../services/deployopts.service';

@Component({
  selector: 'table-subscriptions',
  templateUrl: './table-subscriptions.component.html',
  styleUrls: ['./table-subscriptions.component.scss']
})
export class TableSubsComponent implements OnInit, AfterViewInit {
  
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  
  previous: any = [];
  searchText: string = '';
  maxVisibleItems: number = 25;

  currentUser;
  loading: boolean = false;
  elements = [];

  @HostListener('input') oninput() {
    this.mdbTablePagination.searchText = this.searchText;
  }

  constructor(private modalService: MDBModalService, private cdRef: ChangeDetectorRef, private _subscriptionsService: SubscriptionsService) {  
  }

  private _initElements(): void {
    this.mdbTable.setDataSource(this.elements);
    this.elements = this.mdbTable.getDataSource();
    this.previous = this.mdbTable.getDataSource();
  }

  ngOnInit() {
    this.refreshData();
  }

  refreshData() {
    this.loading = true;
    this.searchText = "";
    var usersSub = this._subscriptionsService.getSubscriptions().subscribe( res => {
      usersSub.unsubscribe();
      this.elements = res.results; 
      this.loading = false;
      this._initElements();
    });
  }

  ngAfterViewInit() {
    
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

  openNewSubsModal( subscription ) {
    var modalRef = this.modalService.show(SubscriptionModalComponent, {
        class: 'modal-md modal-notify',
        containerClass: '',
        data: {
          subscription: subscription
        }
      } );
  
      var sub = modalRef.content.action.subscribe( (data: any) => { 
        sub.unsubscribe();
        
        console.log("new subs data", data);
        this.refreshData();
      });
  }

}