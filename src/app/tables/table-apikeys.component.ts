import { MdbTablePaginationComponent, MdbTableDirective, MDBModalService } from 'angular-bootstrap-md';

import { Component, OnInit, ViewChild, HostListener, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { UsersService } from '../services/users.service';
import { ApikeyModalComponent } from '../modals/edit-apikey.component';
import { ModalConfirmComponent } from '../modals/confirm.component';

@Component({
  selector: 'table-apikeys',
  templateUrl: './table-apikeys.component.html',
  styleUrls: ['./table-apikeys.component.scss']
})
export class TableApiKeysComponent implements OnInit, AfterViewInit {
  
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  
  previous: any = [];
  searchText: string = '';
  maxVisibleItems: number = 25;

  loading: boolean = false;
  elements = [];

  @HostListener('input') oninput() {
    this.mdbTablePagination.searchText = this.searchText;
  }

  constructor(private modalService: MDBModalService, private cdRef: ChangeDetectorRef, private _usersService: UsersService) {  
  }

  private _initElements(): void {
    this.mdbTable.setDataSource(this.elements);
    this.elements = this.mdbTable.getDataSource();
    this.previous = this.mdbTable.getDataSource();
  }

  ngOnInit() {
    this.refreshData();
  }

  refreshData() {
    this.loading = true;
    this.searchText = "";
    var sub = this._usersService.getApiKeys().subscribe( res => {
        sub.unsubscribe();
        this.elements = res.results; 
        this.loading = false;
        this._initElements();
    });
  }

  ngAfterViewInit() {
    
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

  openNewApiKeyModal(apiKey) {
    var modalRef = this.modalService.show(ApikeyModalComponent, {
      class: 'modal-lg modal-notify',
      containerClass: '',
      data: {
        apiKey: apiKey
      }
    } );

    var sub = modalRef.content.action.subscribe( (data: any) => { 
      sub.unsubscribe();
      
      console.log("new api key data", data);
      this.refreshData();
    });
  }

  openConfirmDeleteModal(apiKey) {
    var modalRef = this.modalService.show(ModalConfirmComponent, {
      class: 'modal-sm modal-notify modal-danger',
      containerClass: '',
      data: {
        info: {
          title: 'Confirm revoke?',
          icon: ''
        }
      }
  } );

  var sub = modalRef.content.action.subscribe( (result: any) => { 
    sub.unsubscribe();
    this._usersService.delApikey(apiKey._id).subscribe( res=> {
      console.log("done", res);
      this.refreshData();
    });      
  });
}

}