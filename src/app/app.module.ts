import { BrowserModule } from '@angular/platform-browser';
import { NgModule, SecurityContext } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { HomeComponent } from './home/home.component';
import { ProvisionsComponent } from './provisions/provisions.component';
import { AuthGuard } from './services/auth.guard';
import { ProvisionsService } from './services/provisions.service';
import { ScenariosService } from './services/scenarios.service';
import { UsersService } from './services/users.service';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MarkdownModule, MarkedOptions, MarkedRenderer } from 'ngx-markdown';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { LogsComponent } from './logs/logs.component';
import { ScenariosComponent } from './scenarios/scenarios.component';
import { AdminComponent } from './admin/admin.component';
import { PopoverconfirmComponent } from './popoverconfirm/popoverconfirm.component';
import { FormsModule } from '@angular/forms';
import { MyHttpInterceptor } from './interceptors/http.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TableProvisionsAdminComponent } from './tables/table-provisions.component';
import { TableScenariosComponent } from './tables/table-scenarios.component';
import { TableUsersComponent } from './tables/table-users.component';
import { TableNotificationsComponent } from './tables/table-notifications.component';
import { AlertComponent } from './modals/alert.component';
import { AlertService } from './services/alert.service';
import { ModalInfoComponent } from './modals/modalinfo.component';
import { ModalConfirmComponent } from './modals/confirm.component';
import { FilterPipe } from './filter.pipe';
import { FaqComponent } from './faq/faq.component';
import { NewProvisionConfirmComponent } from './modals/new-provision.component';
import { ScenarioModalComponent } from './modals/edit-scenario.component';
import { SubscriptionModalComponent } from './modals/edit-subscription.component';
import { TableSubsComponent } from './tables/table-subscriptions.component';
import { TableVmTypesComponent } from './tables/table-vmtypes.component';

import { SubscriptionsService } from './services/deployopts.service';
import { TableApiKeysComponent } from './tables/table-apikeys.component';
import { ApikeyModalComponent } from './modals/edit-apikey.component';
import { VmTypeModalComponent } from './modals/edit-vmtype.component';





export function markedOptions(): MarkedOptions {
  const renderer = new MarkedRenderer();

  renderer.blockquote = (text: string) => {
    return '<blockquote class="blockquote"><p>' + text + '</p></blockquote>';
  };

  return { renderer };
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProvisionsComponent,
    LogsComponent,
    ScenariosComponent,
    AdminComponent,
    PopoverconfirmComponent,
    TableProvisionsAdminComponent,
    TableUsersComponent,
    AlertComponent,
    ModalInfoComponent,
    ModalConfirmComponent,
    FilterPipe,
    FaqComponent,
    NewProvisionConfirmComponent,
    TableScenariosComponent,
    TableNotificationsComponent,
    ScenarioModalComponent,
    SubscriptionModalComponent,
    TableSubsComponent,
    TableApiKeysComponent,
    ApikeyModalComponent,
    TableVmTypesComponent,
    VmTypeModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UiModule,
    HttpClientModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    MarkdownModule.forRoot({
      loader: HttpClient
    }),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: MyHttpInterceptor, multi: true },
    ProvisionsService, 
    ScenariosService, 
    SubscriptionsService,
    UsersService, 
    AlertService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
